import 'package:flutter_test/flutter_test.dart';
import 'package:mpay_flutter_library/utils/helpers/url_helper.dart';

void main() {
  test('URL helper test', () {
    final String _encodedResult =
        'https://www.arabbank.com.qa/ar/mainmenu/%D8%A7%D9%84%D8%B5%D9%81%D8%AD%D8%A9-%D8%A7%D9%84%D8%B1%D8%A6%D9%8A%D8%B3%D9%8A%D8%A9/%D8%A7%D9%84%D8%B4%D8%B1%D9%88%D8%B7-%D9%88%D8%A7%D9%84%D8%A3%D8%AD%D9%83%D8%A7%D9%85-%D8%A7%D9%84%D8%AE%D8%A7%D8%B5%D8%A9-%D8%A8%D8%A7%D9%84%D9%85%D8%AD%D9%81%D8%B8%D8%A9-%D8%A7%D9%84%D8%A7%D9%84%D9%83%D8%AA%D8%B1%D9%88%D9%86%D9%8A%D8%A9';
    final String _plainResult = 'https://www.arabbank.com.qa/ar/mainmenu/';

    String _url = UrlHelper.shared.encodeUrl(
        'https://www.arabbank.com.qa/ar/mainmenu/الصفحة-الرئيسية/الشروط-والأحكام-الخاصة-بالمحفظة-الالكترونية');
    expect(_url == _encodedResult, true);

    _url = UrlHelper.shared.encodeUrl(_plainResult);
    expect(_url == _plainResult, true);

    _url = UrlHelper.shared.encodeUrl(_encodedResult);
    expect(_url == _encodedResult, true);
  });
}
