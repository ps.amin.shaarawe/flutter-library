import 'package:flutter_test/flutter_test.dart';
import 'package:mpay_flutter_library/utils/helpers/mobile_number_helper.dart';

void main() {
  test('Mobile number helper test 974', () {
    final testMobile = '0097433055339';
    final testMobile2 = '0097455055339';
    final testMobile3 = '0097446812483';
    final _countryCode = '+974';

    final _mob1 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '33055 339', countryCode: _countryCode);
    final _mob2 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '0097433 055339', countryCode: _countryCode);
    final _mob3 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '+974 33055339', countryCode: _countryCode);
    final _mob4 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '3305 5339', countryCode: _countryCode);
    final _mob5 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '97433055339', countryCode: _countryCode);
    final _mob6 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '97455055339', countryCode: _countryCode);
    final _mob7 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '974 550 55 339', countryCode: _countryCode);
    final _mob8 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '0097455055339', countryCode: _countryCode);
    final _mob9 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '+97455055339', countryCode: _countryCode);
    final mob10 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '+ 97455055339', countryCode: _countryCode);
    final mob11 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '+974 55 055 339', countryCode: _countryCode);
    final mob12 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '+974 550 55 339', countryCode: _countryCode);
    final mob13 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '055055339', countryCode: _countryCode);
    final mob14 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '033055339', countryCode: _countryCode);
    final mob15 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '046812483', countryCode: _countryCode);
    final mob16 = MobileNumberHelper.shared.generateValidMobileNumber(
        mobileNumber: '46812483', countryCode: _countryCode);

    expect(testMobile == _mob1, true);
    expect(testMobile == _mob2, true);
    expect(testMobile == _mob3, true);
    expect(testMobile == _mob4, true);
    expect(testMobile == _mob5, true);
    expect(testMobile2 == _mob6, true);
    expect(testMobile2 == _mob7, true);
    expect(testMobile2 == _mob8, true);
    expect(testMobile2 == _mob9, true);
    expect(testMobile2 == mob10, true);
    expect(testMobile2 == mob11, true);
    expect(testMobile2 == mob12, true);
    expect(testMobile2 == mob13, true);
    expect(testMobile == mob14, true);
    expect(testMobile3 == mob15, true);
    expect(testMobile3 == mob16, true);
  });

}
