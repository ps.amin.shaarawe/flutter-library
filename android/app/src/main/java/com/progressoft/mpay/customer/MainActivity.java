package com.progressoft.mpay.customer;

import android.util.Base64;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import io.flutter.embedding.android.FlutterFragmentActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterFragmentActivity {
    private static final String MAIN_CHANNEL = "MAIN_CHANNEL";
    private static final String METHOD_SIGN_TEXT = "METHOD_SIGN_TEXT";
    private static final String METHOD_GET_PUBLIC_KEY = "METHOD_GET_PUBLIC_KEY";
    private static final String METHOD_ENCRYPT_RSA = "METHOD_ENCRYPT_RSA";
    private static final String METHOD_DECRYPT_RSA = "METHOD_DECRYPT_RSA";
    private static final String METHOD_ENABLE_SCREEN_SHOT_PREVENT = "METHOD_ENABLE_SCREEN_SHOT_PREVENT";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        initMainChannel(flutterEngine);
    }

    private void initMainChannel(FlutterEngine flutterEngine) {
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), MAIN_CHANNEL).setMethodCallHandler((methodCall, result) -> {

            if (methodCall.method.equals(METHOD_SIGN_TEXT)) {
                String plainText = methodCall.arguments.toString();
                String signed = SiginingHelper.signingSecureHash(plainText, this);
                result.success(signed);
            }

            if (methodCall.method.equals(METHOD_ENABLE_SCREEN_SHOT_PREVENT)) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
                result.success(true);
            }

            if (methodCall.method.equals(METHOD_GET_PUBLIC_KEY)) {
                try {
                    PublicKey publicKey = SiginingHelper.getInstance(this).getPublicKey();
                    String publicKeyString = Base64.encodeToString(publicKey.getEncoded(), Base64.NO_WRAP);
                    result.success(publicKeyString);
                } catch (CertificateException | IOException | KeyStoreException | NoSuchAlgorithmException | KeyManagementException | InvalidAlgorithmParameterException | NoSuchProviderException | InvalidKeyException | UnrecoverableEntryException value) {
                    value.printStackTrace();
                    result.success("");
                }
            }

            if (methodCall.method.equals(METHOD_ENCRYPT_RSA)) {
                try {
                    String encrypted = SiginingHelper.getInstance(this).encrypt(methodCall.arguments.toString());
                    result.success(encrypted);
                } catch (Exception e) {
                    e.printStackTrace();
                    result.success("");
                }
            }

            if (methodCall.method.equals(METHOD_DECRYPT_RSA)) {
                try {
                    String decrypted = SiginingHelper.getInstance(this).decrypt(methodCall.arguments.toString());
                    result.success(decrypted);
                } catch (Exception e) {
                    e.printStackTrace();
                    result.success("");
                }
            }
        });
    }

}
