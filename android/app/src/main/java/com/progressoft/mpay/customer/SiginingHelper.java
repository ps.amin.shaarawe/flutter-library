package com.progressoft.mpay.customer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.x500.X500Principal;

public class SiginingHelper {
    private static SiginingHelper instance = null;
    private static String keyAlias = "RSASigningAlias";
    private static Context context;
    private PublicKey publicKey = null;
    private PrivateKey privateKey = null;

    @SuppressLint("WrongConstant")
    private SiginingHelper() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException, NoSuchProviderException, InvalidAlgorithmParameterException, InvalidKeyException, UnrecoverableEntryException {
        Locale initialLocale = Locale.getDefault();
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load((KeyStore.LoadStoreParameter) null);
            if (!keyStore.containsAlias(keyAlias)) {
                Calendar notBefore = Calendar.getInstance();
                Calendar notAfter = Calendar.getInstance();
                notAfter.add(1, 1);
                KeyPairGeneratorSpec spec = (new KeyPairGeneratorSpec.Builder(context)).setAlias(keyAlias).setSubject(new X500Principal("CN=test")).setSerialNumber(BigInteger.ONE).setStartDate(notBefore.getTime()).setEndDate(notAfter.getTime()).build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
                generator.initialize(spec);
                generator.generateKeyPair().getPublic();
            }
            this.setLocale(initialLocale);

            this.publicKey = keyStore.getCertificate(keyAlias).getPublicKey();
            this.privateKey = (PrivateKey) keyStore.getKey(keyAlias, (char[]) null);
        } catch (Exception var8) {
            this.setLocale(initialLocale);
            throw new RuntimeException("Can't generate keyStore Logs: " + var8.getMessage(), var8);
        }
    }

    public static synchronized SiginingHelper getInstance(Context contexts) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException, InvalidAlgorithmParameterException, NoSuchProviderException, InvalidKeyException, UnrecoverableEntryException {
        if (instance == null) {
            context = contexts;
            instance = new SiginingHelper();
        }
        return instance;
    }

    public static String signingSecureHash(String secureHash, final Context context) {
        SiginingHelper keyStore = null;

        try {
            keyStore = SiginingHelper.getInstance(context);
        } catch (CertificateException var9) {
            var9.printStackTrace();
        } catch (IOException value) {
            value.printStackTrace();
        } catch (KeyStoreException value) {
            value.printStackTrace();
        } catch (NoSuchAlgorithmException value) {
            value.printStackTrace();
        } catch (KeyManagementException value) {
            value.printStackTrace();
        } catch (InvalidKeyException value) {
            value.printStackTrace();
        } catch (UnrecoverableEntryException value) {
            value.printStackTrace();
        } catch (InvalidAlgorithmParameterException value) {
            value.printStackTrace();
        } catch (NoSuchProviderException value) {
            value.printStackTrace();
        }

        try {
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(keyStore.getPrivateKey());
            signature.update(secureHash.getBytes("UTF-8"));
            byte[] signedBytes = signature.sign();
            final String base64 = Base64.encodeToString(signedBytes, 2);
            return base64;
        } catch (NoSuchAlgorithmException value) {
            value.printStackTrace();
        } catch (InvalidKeyException value) {
            value.printStackTrace();
        } catch (SignatureException value) {
            value.printStackTrace();
        } catch (UnsupportedEncodingException value) {
            value.printStackTrace();
        }

        return null;
    }

    private void setLocale(Locale locale) {
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration config = resources.getConfiguration();
        config.locale = locale;
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public String encrypt(String data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] bytes = cipher.doFinal(data.getBytes());
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public String decrypt(String data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] encryptedData = Base64.decode(data, Base64.DEFAULT);
        byte[] decodedData = cipher.doFinal(encryptedData);
        return new String(decodedData);
    }
}