import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/extra_data_helper.dart';
import 'package:mpay_flutter_library/ps_mpay_core/managers/connection_manager.dart';
import 'package:mpay_flutter_library/ps_mpay_core/managers/http_configurations.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/extra_data_object.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/operation_request_object.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/operation_response_object.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';

enum ImageType {
  SELFIEIMAGE,
  IDFACEIMAGE,
  IDBACKIMAGE,
  PASSPORTIMAGE,
  OTHER,
}

class MediaBusinessManager {
  void uploadImage(
    BuildContext context,
    String filePath,
    ImageType imageType,
    ResponseCompletionHandler completionHandler, {bool isHashed = false}
  ) async {
    String key =
        await SecureStorageHelper().read(key: CacheKeys.devicePublicKey);

    if (SharedParam.shared.isDebug) {
      print('public key = ' + key.toString());
    }

    final extraData = [
      ExtraDataHelper.shared.getExtraData(key: 'section', value: 'ALL'),
      ExtraDataHelper.shared.getExtraData(
          key: 'imageType', value: (imageType.index + 1).toString())
    ];

    final operationRequest =
        OpreationRequestObject().setup(extraData: extraData);

    ConnectionManager().sendRequestToServer(
      operationRequest: operationRequest,
      context: context,
      showLoading: false,
      isReload: true,
      isHashed: isHashed,
      httpMethod: HttpMethod.multipart,
      filePath: filePath,
      completionHandler: ({errorCode, result, status, statusCode}) {
        if (status && result.length > 0) {
          OperationResponseObject body = result[0];

          if (body.response.extraData.length > 0) {
            ExtraDataObject extraData = body.response.extraData[0];

            completionHandler('', [extraData.value], true);
          } else {
            completionHandler(statusCode, [], false);
          }
        } else {
          completionHandler(errorCode, [], false);
        }
      },
    );
  }
}
