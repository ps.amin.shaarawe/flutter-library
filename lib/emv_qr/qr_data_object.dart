class QrDataObject {
  String fieldTag;
  int length;
  List<QrDataObject> subFields;
  String value;

  QrDataObject({this.fieldTag, this.length, this.subFields, this.value});

  QrDataObject.fromJson(Map<String, dynamic> json) {
    fieldTag = json['fieldTag'];
    length = json['length'];
    if (json['subFields'] != null) {
      subFields = [];
      json['subFields'].forEach((v) {
        subFields.add(new QrDataObject.fromJson(v));
      });
    }
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fieldTag'] = this.fieldTag;
    data['length'] = this.length;
    if (this.subFields != null) {
      data['subFields'] = this.subFields.map((v) => v.toJson()).toList();
    }
    data['value'] = this.value;
    return data;
  }

  @override
  String toString() {
    return 'Tag: $fieldTag, value: $value';
  }
}
