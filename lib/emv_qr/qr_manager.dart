import 'dart:core';

int lengthCount = 2;
int tagCount = 2;

class QrManager {
  static var shared = QrManager();

  List<QrFieldObject> parse(String input) {
    List<QrFieldObject> parsedData = [];

    String data = input;

    while (data.length > 0) {
      List parsedResult = this.parseData(input: data);

      if (parsedResult.length == 0) {
        return [];
      }

      QrFieldObject mainObject = parsedResult[0];
      data = parsedResult[1];

      List<String> subFieldsIds = ['26', '62', '27', '80'];

      if (subFieldsIds.contains(mainObject.fieldTag)) {
        var subData = mainObject.value;
        List<QrFieldObject> subFields = [];
        while (subData.length > 0) {
          var parsedResult = this.parseData(input: subData, isSubFields: true);

          if (parsedResult.length == 0) {
            return [];
          }
          var subObject = parsedResult[0];
          subData = parsedResult[1];
          subFields.add(subObject);
        }

        mainObject.subFields = subFields;
      }

      parsedData.add(mainObject);
    }

    return parsedData;
  }

  List parseData({String input = '', bool isSubFields = false}) {
    String data = input;
    QrFieldObject object = QrFieldObject();

    if (data.length < tagCount) {
      return [];
    }

    String fieldTag = data.substring(0, tagCount);

    if (fieldTag == null) {
      return [];
    }

    if (isSubFields) {
      object.subFieldTag = fieldTag;
    } else {
      object.fieldTag = fieldTag;
    }

    if (data.length < tagCount + lengthCount) {
      return [];
    }

    var lengthString = data.substring(tagCount, tagCount + lengthCount);
    var length = int.tryParse(lengthString);

    if (length == null) {
      return [];
    }

    object.length = length;

    if (data.length < length + tagCount + lengthCount) {
      return [];
    }

    String value =
        data.substring(tagCount + lengthCount, length + tagCount + lengthCount);

    if (value == null) {
      return [];
    }

    object.value = value;

    if (data.length <= 0) {
      return [];
    }

    data = data.substring(length + tagCount + lengthCount, data.length);

    if (data == null) {
      return [];
    }

    return [object, data];
  }

  String generateQrData({List<QrFieldObject> inputArray}) {
    var finalQrData = '';

    for (int mainCounter = 0; mainCounter < inputArray.length; mainCounter++) {
      QrFieldObject mainObject = inputArray[mainCounter];

      var subFields = '';
      var finalLength = 0;

      if (mainObject.subFields != null) {
        for (int subCounter = 0;
            subCounter < mainObject.subFields.length;
            subCounter++) {
          QrFieldObject subObject = mainObject.subFields[subCounter];

          subObject.length = subObject.value.length;

          var length = subObject.length.toString();

          if (subObject.length < 10) {
            length = '0' + subObject.length.toString();
          }

          finalLength = finalLength + subObject.length + tagCount + lengthCount;
          subFields =
              subFields + subObject.subFieldTag + length + subObject.value;
        }
      }

      if (mainObject.subFields != null) {
        mainObject.length = 0;
        mainObject.value = '';
      } else {
        mainObject.length = mainObject.value.length;
      }

      finalLength = finalLength + mainObject.length;

      var length = finalLength.toString();

      if (finalLength < 10) {
        length = '0' + finalLength.toString();
      }

      finalQrData = finalQrData +
          mainObject.fieldTag +
          length +
          mainObject.value +
          subFields;
    }

    return finalQrData;
  }

  String findValue(
      {List<QrFieldObject> array, String mainTag, String subFieldTag = ''}) {
    if (subFieldTag.length > 0) {
      var filteredArray = array
          .where((fieldObject) => fieldObject.fieldTag == mainTag)
          .toList();

      if (filteredArray.length > 0) {
        var subFields = filteredArray[0].subFields;
        var subArray = subFields
            .where((fieldObject) => fieldObject.subFieldTag == subFieldTag)
            .toList();

        if (subArray.length > 0) {
          return subArray[0].value;
        }
      }

      return '';
    }

    var filteredArray =
        array.where((fieldObject) => fieldObject.fieldTag == mainTag).toList();

    if (filteredArray.length > 0) {
      return filteredArray[0].value;
    }

    return '';
  }
}

class QrFieldObject {
  String fieldTag = '';
  int length = 0;
  String value = '';
  String subFieldTag = '';

  List<QrFieldObject> subFields;

  QrFieldObject(
      {this.fieldTag = '',
      this.value = '',
      this.subFieldTag = '',
      this.subFields});
}
