import 'package:flutter/cupertino.dart';
import 'package:mpay_flutter_library/business_managers/media_business_manager.dart';

import 'image_upload_interactor.dart';

mixin ImageUploading implements IOnImageUploadingListener {
  ImageUploadingInterActor _imageUploadingInterActor =
      ImageUploadingInterActor();

  uploadImage(BuildContext context, String filePath, ImageType imageType,
      {bool isHashed}) {
    //validation
    _imageUploadingInterActor.uploadImage(context, filePath, imageType, this,
        isHashed: isHashed);
  }
}

//
abstract class IOnImageUploadingListener {
  void onUploadSuccess(String imageId);

  void onUploadError(String message);
}
