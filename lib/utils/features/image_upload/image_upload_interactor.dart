import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/business_managers/media_business_manager.dart';

import 'image_upload_presenter_mixin.dart';

class ImageUploadingInterActor {
  uploadImage(BuildContext context, String filePath, ImageType imageType,
      IOnImageUploadingListener listener, {bool isHashed = true}) {
    MediaBusinessManager().uploadImage(context, filePath, imageType,
        (message, resultArray, status) {
      if (status)
        listener.onUploadSuccess(resultArray[0]);
      else
        listener.onUploadError(message);
    }, isHashed: isHashed);
  }
}
