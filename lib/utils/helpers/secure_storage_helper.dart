import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mpay_flutter_library/utils/helpers/aes_helper.dart';

class SecureStorageHelper {
  Future<String> read({@required String key}) async {
    try {
      final storage = new FlutterSecureStorage();
      String keyEnc = AesHelper.encrypt(AesHelper.getKey(), key);
      String value = await storage.read(key: keyEnc);

      if (value == null) {
        value = '';
      }

      return AesHelper.decrypt(AesHelper.getKey(), value);
    } catch (e) {}

    return '';
  }

  Future<void> save({@required String key, @required String value}) async {
    try {
      final storage = new FlutterSecureStorage();
      String keyEnc = AesHelper.encrypt(AesHelper.getKey(), key);
      String valueEnc = AesHelper.encrypt(AesHelper.getKey(), value);
      await storage.write(key: keyEnc, value: valueEnc);
    } catch (e) {}
  }

  Future<void> clear() async {
    try {
      final storage = new FlutterSecureStorage();
      await storage.deleteAll();
    } catch (e) {}
  }
}
