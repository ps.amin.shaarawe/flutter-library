import 'package:flutter/material.dart';

import 'flutter_money_formatter/src/flutter_money_formatter_base.dart';
import 'flutter_money_formatter/src/utils/compact_format_type.dart';
import 'flutter_money_formatter/src/utils/money_formatter_settings.dart';

class FormateHelper {
  static var shared = FormateHelper();

  String getIntAmountFormatted({
    @required int original,
    int fractionDigits = 3,
    String thousandSeparator = ',',
    String decimalSeparator = '.',
    CompactFormatType compactFormatType = CompactFormatType.long,
  }) {
    try {
      double amount = original.toDouble();
      return _getFormate(
        amount: amount,
        fractionDigits: fractionDigits,
        thousandSeparator: thousandSeparator,
        decimalSeparator: decimalSeparator,
        compactFormatType: compactFormatType,
      );
    } catch (e) {}

    return original.toString();
  }

  String getStringAmountFormatted({
    @required String original,
    int fractionDigits = 3,
    String thousandSeparator = ',',
    String decimalSeparator = '.',
    CompactFormatType compactFormatType = CompactFormatType.long,
  }) {
    if (original == '') {
      original = '0.0';
    }

    try {
      double amount = double.tryParse(original);
      return _getFormate(
        amount: amount,
        fractionDigits: fractionDigits,
        thousandSeparator: thousandSeparator,
        decimalSeparator: decimalSeparator,
        compactFormatType: compactFormatType,
      );
    } catch (e) {}

    return original;
  }

  String getDoubleAmountFormatted({
    @required double original,
    int fractionDigits = 3,
    String thousandSeparator = ',',
    String decimalSeparator = '.',
    CompactFormatType compactFormatType = CompactFormatType.long,
  }) {
    try {
      double amount = original;
      return _getFormate(
        amount: amount,
        fractionDigits: fractionDigits,
        thousandSeparator: thousandSeparator,
        decimalSeparator: decimalSeparator,
        compactFormatType: compactFormatType,
      );
    } catch (e) {}

    return original.toString();
  }

  String _getFormate({
    @required double amount,
    @required int fractionDigits,
    @required String thousandSeparator,
    @required String decimalSeparator,
    @required CompactFormatType compactFormatType,
  }) {
    try {
      final formatter = FlutterMoneyFormatter(
          amount: amount,
          settings: MoneyFormatterSettings(
            thousandSeparator: thousandSeparator,
            decimalSeparator: decimalSeparator,
            compactFormatType: compactFormatType,
            fractionDigits: fractionDigits,
          ));
      return formatter.output.nonSymbol;
    } catch (e) {}

    return amount.toString();
  }

  String getCounterText({@required int seconds}) {
    String twoDigits(int n) {
      if (n >= 10) return '$n';
      return '0$n';
    }

    Duration duration = Duration(seconds: seconds);
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));

    return '$twoDigitMinutes:$twoDigitSeconds';
  }
}
