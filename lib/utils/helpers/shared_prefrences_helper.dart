import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  Future<String> read({@required String key}) async {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getString(key) ?? '';

    if (value == null) {
      return '';
    }

    return value;
  }

  Future<bool> readBoolean({@required String key}) async {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getBool(key) ?? '';

    if (value == null || value == '') {
      return true;
    }

    return value;
  }

  Future<void> save({@required String key, @required String value}) async {
    if (key == null || value == null) {
      return;
    }

    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  Future<void> saveBoolean({@required String key, @required bool value}) async {
    if (key == null || value == null) {
      return;
    }

    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
  }

  Future<void> clear() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      await prefs.clear();
    } catch (e) {}
  }
}
