import 'package:flutter/material.dart';

class GradientPainter extends CustomPainter {
  GradientPainter({this.gradient, this.strokeWidth, this.borderRadius});
  final Gradient gradient;
  final double strokeWidth;
  final double borderRadius;
  final Paint paintObject = Paint();
  @override
  void paint(Canvas canvas, Size size) {
    Rect innerRect = Rect.fromLTRB(strokeWidth, strokeWidth, size.width - strokeWidth, size.height - strokeWidth);
    RRect innerRoundedRect = RRect.fromRectAndRadius(innerRect, Radius.circular(borderRadius));
    Rect outerRect = Offset.zero & size;
    RRect outerRoundedRect = RRect.fromRectAndRadius(outerRect, Radius.circular(borderRadius));
    paintObject.shader = gradient.createShader(outerRect);
    Path borderPath = _calculateBorderPath(outerRoundedRect, innerRoundedRect);
    canvas.drawPath(borderPath, paintObject);
  }
  Path _calculateBorderPath(RRect outerRRect, RRect innerRRect) {
    Path outerRectPath = Path()..addRRect(outerRRect);
    Path innerRectPath = Path()..addRRect(innerRRect);
    return Path.combine(PathOperation.difference, outerRectPath, innerRectPath);
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
class GradientBorderContainer extends StatelessWidget {
  GradientBorderContainer({
    @required gradient,
    @required this.child,
    @required this.onPressed,
    this.strokeWidth = 2,
    this.borderRadius = 64,
    this.padding = 8
  }) : this.painter = GradientPainter(
      gradient: gradient, strokeWidth: strokeWidth, borderRadius: borderRadius
  );
  final GradientPainter painter;
  final Widget child;
  final VoidCallback onPressed;
  final double strokeWidth;
  final double borderRadius;
  final double padding;
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        painter: painter,
        child: Container(
            padding: EdgeInsets.all(padding),
            child: child
        )
    );
  }
}