
import 'package:flutter/material.dart';

class StringHelper {
  static final shared = StringHelper();
 
  String replaceArabicNumbers({@required String containsArabic}) {
    return containsArabic
        .replaceAll('٠', '0')
        .replaceAll('١', '1')
        .replaceAll('٢', '2')
        .replaceAll('٣', '3')
        .replaceAll('٤', '4')
        .replaceAll('٥', '5')
        .replaceAll('٦', '6')
        .replaceAll('٧', '7')
        .replaceAll('٨', '8')
        .replaceAll('٩', '9')
        .replaceAll('۰',
            '0') // dont remove the duplicate please (asci code is different)
        .replaceAll('۱', '1')
        .replaceAll('۲', '2')
        .replaceAll('۳', '3')
        .replaceAll('۴', '4')
        .replaceAll('۵', '5')
        .replaceAll('۶', '6')
        .replaceAll('۷', '7')
        .replaceAll('۸', '8')
        .replaceAll('۹', '9')
        .replaceAll('*', '')
        .replaceAll('#', '')
        .replaceAll(',', '')
        .replaceAll(';', '')
        .replaceAll('+', '')
        .replaceAll('٫', '.');
  }
}
