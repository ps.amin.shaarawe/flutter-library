import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/formate_helper.dart';
import 'package:mpay_flutter_library/utils/helpers/string_helper.dart';

class AmountFormatHelper {
  static AmountFormatHelper shared = AmountFormatHelper();

  String format({
    @required String amount,
    @required String currency,
    @required int fractionDigits,
  }) {
    var stringAmountFormatted = FormateHelper.shared.getStringAmountFormatted(
        original: amount, fractionDigits: fractionDigits);
    if (PsLibrarySetupObject.shared.isArabic) return stringAmountFormatted + ' ' + currency;

    return currency + ' ' + stringAmountFormatted;
  }

  bool isValidAmount(String amountStr) {
    if (amountStr == null || amountStr.trim().isEmpty) return false;
    double amount = 0.0;
    try {
      amount = double.parse(
          StringHelper.shared.replaceArabicNumbers(containsArabic: amountStr));
      amount = double.parse(amount.toString());
    } catch (e) {
      return false;
    }
    if (amount <= 0) return false;

    return true;
  }
}
