import 'package:flutter/material.dart';

class GradientMask extends StatelessWidget {
  GradientMask({this.child, this.gradient});

  final Widget child;
  final Gradient gradient;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        return gradient.createShader(bounds);
      },
      child: child,
    );
  }
}
