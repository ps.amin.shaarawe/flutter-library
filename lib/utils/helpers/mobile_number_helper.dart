import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/utils/helpers/string_helper.dart';

class MobileNumberHelper {
  static final shared = MobileNumberHelper();

  String generateValidMobileNumber({
    @required String mobileNumber,
    @required String countryCode,
  }) {
    var _countryCode = countryCode.replaceAll('+', '');
    mobileNumber =
        StringHelper.shared.replaceArabicNumbers(containsArabic: mobileNumber);
    if (mobileNumber.length > 0 && mobileNumber.length <= 10) {
      final firstChar = mobileNumber.substring(0, 1);

      if (firstChar == '0') {
        mobileNumber = mobileNumber.substring(1, mobileNumber.length);
      }
    }

    mobileNumber = mobileNumber.replaceAll(' ', '');
    mobileNumber = mobileNumber.replaceAll(' ', '');
    mobileNumber = mobileNumber.replaceAll('(', '');
    mobileNumber = mobileNumber.replaceAll(')', '');
    mobileNumber = mobileNumber.replaceAll('-', '');
    mobileNumber = mobileNumber.replaceAll('+', '00');
    mobileNumber = mobileNumber.replaceAll('\t', '');
    mobileNumber = mobileNumber.replaceAll('\n', '');

    if (mobileNumber.startsWith(_countryCode))
      mobileNumber =
          mobileNumber.replaceFirst(_countryCode, '00' + _countryCode);

    if (mobileNumber.length < 10) {
      mobileNumber = '00' + _countryCode + mobileNumber;
    }

    mobileNumber = StringHelper.shared.replaceArabicNumbers(containsArabic: mobileNumber);

    return mobileNumber;
  }
}
