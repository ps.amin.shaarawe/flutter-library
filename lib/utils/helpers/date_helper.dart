import 'package:intl/intl.dart';

class DateHelper {
  static DateHelper shared = DateHelper();

  String convertDateStringFormat(
      {String dateStr, String inputFormate, String outFormate}) {
    try {
      if (dateStr.length == 0) {
        return '';
      }
      DateFormat dateFormatter = new DateFormat(outFormate, 'en');
      DateTime dateTime = new DateFormat(inputFormate, 'en').parse(dateStr);

      String formattedDate = dateFormatter.format(dateTime);
      return formattedDate;
    } catch (error) {
      return '';
    }
  }

  String convertDateFormatToString({DateTime date, String outFormate}) {
    try {
      DateFormat dateFormatter = DateFormat(outFormate, 'en');
      return dateFormatter.format(date);
    } catch (error) {
      return '';
    }
  }

  DateTime convertDateFormatToDate(
      {DateTime date, String inputFormate, String outFormate}) {
    try {
      DateFormat dateFormatter = DateFormat(outFormate, 'en');
      String dateStr = dateFormatter.format(date);
      DateTime dateTime = new DateFormat(inputFormate, 'en').parse(dateStr);
      return dateTime;
    } catch (error) {
      return null;
    }
  }

  DateTime getDateFromString({String dateStr, String inputFormate}) {
    try {
      var dateTime = new DateFormat(inputFormate, 'en').parse(dateStr);
      return dateTime;
    } catch (error) {
      return null;
    }
  }
}
