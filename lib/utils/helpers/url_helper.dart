import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlHelper {
  static final UrlHelper shared = UrlHelper();

  String encodeUrl(String url) {
    if (url.contains('%')) return url;
    String _url = Uri.encodeFull(url);
    _url = urlEncode(text: _url);
    return _url;
  }

  String urlEncode({@required String text}) {
    String output = text;

    var detectHash = text.contains('#');
    var detectAnd = text.contains('&');
    var detectSlash = text.contains('/');

    if (detectHash == true) {
      output = output.replaceAll('#', '%23');
    }

    if (detectAnd == true) {
      output = output.replaceAll('#', '%26');
    }

    if (detectSlash == true) {
      output = output.replaceAll('#', '%2F');
    }

    return output;
  }

  Future<void> hitLink({@required String url}) async {
    if(!url.contains("https://") && !url.contains("http://"))
      url="https://"+url;
    String _url = encodeUrl(url);
    if (_url != null) {
      await launch(_url);
    } else {
      throw 'Could not launch $_url';
    }
  }
}
