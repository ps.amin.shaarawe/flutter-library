
import 'package:mpay_flutter_library/configurations/cache_keys.dart';

import 'helpers/secure_storage_helper.dart';
import 'helpers/shared_prefrences_helper.dart';

class ClearCache {

  Future<void> clearCacheStorage() async {
    final _isFirstOpen =
        await SharedPreferencesHelper().readBoolean(key: CacheKeys.firstOpen);

    if (_isFirstOpen) {
      await SecureStorageHelper().clear();
      await SharedPreferencesHelper().clear();
      await SharedPreferencesHelper()
          .saveBoolean(key: CacheKeys.firstOpen, value: false);
    }

  }
}
