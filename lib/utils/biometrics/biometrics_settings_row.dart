import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';
import 'package:mpay_flutter_library/utils/helpers/size_config.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/alert_helper.dart';
import 'package:mpay_flutter_library/utils/ui/textfield_helper.dart';
import 'package:open_app_settings/open_app_settings.dart';

import 'biometrics_manager.dart';

typedef void BiometricsSettingsRowCompletionHandler(
    BiometricsStatus authStatus, String pinCode);

class BiometricsSettingsRow extends StatefulWidget {
  final String allowFingerprintLoginTitle;
  final String allowFaceIdprintLoginTitle;
  final String allowTouchIdprintLoginTitle;
  final String setupBiometricsWarningTitle;
  final String biometricsWarningTitle;
  final String enterPinCodeToContinueTitle;
  final String openSettingsTitle;
  final String pinCodeInvalidTitle;
  final String pinCodeTitle;
  final String pinCodeRequiredTitle;
  final int pinBoxesCount;
  final EdgeInsetsGeometry rowPadding;
  final BiometricsSettingsRowCompletionHandler completionHandler;

  BiometricsSettingsRow({
    Key key,
    @required this.pinCodeTitle,
    @required this.allowFingerprintLoginTitle,
    @required this.allowFaceIdprintLoginTitle,
    @required this.allowTouchIdprintLoginTitle,
    @required this.setupBiometricsWarningTitle,
    @required this.biometricsWarningTitle,
    @required this.enterPinCodeToContinueTitle,
    @required this.openSettingsTitle,
    @required this.pinCodeInvalidTitle,
    @required this.pinCodeRequiredTitle,
    @required this.pinBoxesCount,
    @required this.completionHandler,
    this.rowPadding = const EdgeInsets.all(20),
  }) : super(key: key);

  @override
  _BiometricsSettingsRowState createState() => _BiometricsSettingsRowState();
}

class _BiometricsSettingsRowState extends State<BiometricsSettingsRow> {
  var _pinCodeController = TextEditingController();
  var _pinError = '';
  var _pinFocusNode = FocusNode();
  bool _isAllowedBiometrics = false;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (ctx, snapshot) {
        if (snapshot.hasData) return snapshot.data;
        return Container();
      },
      future: _getBiometrecsRow(),
    );
  }

  Future<Widget> _getBiometrecsRow() async {
    final _token =
        await SecureStorageHelper().read(key: CacheKeys.biometricsLoginToken);
    _isAllowedBiometrics = _token.isNotEmpty;

    var _title = widget.allowFingerprintLoginTitle;

    if (Platform.isIOS) {
      final _availabelLocalAuth =
          await BiometricsManager.shared.getAvailableBiometrics();

      if (_availabelLocalAuth == AvailableBiometrics.face) {
        _title = widget.allowFaceIdprintLoginTitle;
      } else {
        _title = widget.allowTouchIdprintLoginTitle;
      }
    } else {
      _title = widget.allowFingerprintLoginTitle;
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsetsDirectional.only(
              start: 25.0,
              end: 20.0,
              top: 20.0,
              bottom: 20.0,
          ),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.70,
            child: Text(
              _title,
              maxLines: 2,
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                  fontWeight: FontWeight.normal),
            ),
          ),
        ),
        InkWell(
          child: CupertinoSwitch(
            activeColor: Theme.of(context).primaryColor,
            onChanged: _onChangeBiometricSwitchValue,
            value: _isAllowedBiometrics,
          ),
          onTap: () {
            _onChangeBiometricSwitchValue(!_isAllowedBiometrics);
          },
        ),
      ],
    );
  }

  void _onChangeBiometricSwitchValue(bool value) async {
    _pinCodeController.text = '';
    final _availabelLocalAuth =
        await BiometricsManager.shared.getAvailableBiometrics();

    if (_availabelLocalAuth == AvailableBiometrics.none) {
      AlertHelper.shared.showAlertDialog(
        message: widget.setupBiometricsWarningTitle,
        context: context,
        isCancelButton: true,
        buttonLabel: widget.openSettingsTitle,
        cancelCompletionHandler: () {},
        completionHandler: () async {
          await OpenAppSettings.openSecuritySettings();
        },
      );
      return;
    }

    if (value) {
      AlertHelper.shared.showAlertDialog(
          message: widget.biometricsWarningTitle +
              '\n' +
              widget.enterPinCodeToContinueTitle +
              '\n',
          context: context,
          isCancelButton: true,
          buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
          cancelCompletionHandler: () {
            _pinCodeController.text = '';
          },
          customWidget: _getPINTextField(),
          completionHandler: () async {
            if (_pinCodeController.text.length == widget.pinBoxesCount) {
              _startAuth();
            } else {
              AlertHelper.shared.showAlertDialog(
                  message: widget.pinCodeInvalidTitle,
                  context: context,
                  buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
                  isCancelButton: true,
                  cancelCompletionHandler: () {},
                  completionHandler: () async {
                    _onChangeBiometricSwitchValue(value);
                  });
            }
          });
    } else {
      AlertHelper.shared.showAlertDialog(
          message: PsLibrarySetupObject.shared.generalConfirmationMessage,
          context: context,
          isCancelButton: true,
          buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
          cancelCompletionHandler: () {},
          completionHandler: () async {
            await SecureStorageHelper()
                .save(key: CacheKeys.biometricsLoginToken, value: '');
            setState(() {});
          });
    }
  }

  void _startAuth() async {
    final _authStatus =
        await BiometricsManager.shared.authenticateWithBiometrics();
    switch (_authStatus) {
      case BiometricsStatus.authenticated:
        if (widget.completionHandler == null)
          throw Exception(
              'BiometricsButton completionHandler should not be null');

        widget.completionHandler(_authStatus, _pinCodeController.text);
        break;
      case BiometricsStatus.denied:
        break;
      case BiometricsStatus.needAction:
        AlertHelper.shared.showAlertDialog(
            buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
            message: widget.setupBiometricsWarningTitle,
            context: context,
            completionHandler: () {},
            alertType: AlertType.ERROR);
        break;
    }

    setState(() {});
  }

  Widget _getPINTextField() {
    return TextFieldHelper.shared.createGradientField(
      keyboardType: TextInputType.number,
      controller: _pinCodeController,
      focusNode: _pinFocusNode,
      context: context,
      hint: widget.pinCodeRequiredTitle,
      errorText: _pinError,
      onChanged: (value) {
        if (value.length == widget.pinBoxesCount) {
          FocusScope.of(context).requestFocus(new FocusNode());
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        }
      },
      inputFormatters: [
        FilteringTextInputFormatter(new RegExp(' '), allow: false),
        LengthLimitingTextInputFormatter(widget.pinBoxesCount),
      ],
      labelText: widget.pinCodeTitle,
      onSubmitted: (v) {
        FocusScope.of(context).requestFocus(new FocusNode());
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      prefixIcon: Icon(Icons.dialpad, color: Colors.white),
      obscureText: true,
    );
  }
}
