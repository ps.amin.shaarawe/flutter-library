import 'dart:io';
import 'package:flutter/services.dart';
import 'package:local_auth/error_codes.dart' as auth_error;
import 'package:local_auth/local_auth.dart';

enum AvailableBiometrics { face, fingerprint, none }
enum BiometricsStatus { authenticated, denied, needAction }

class BiometricsManager {
  static final shared = BiometricsManager();

  Future<AvailableBiometrics> getAvailableBiometrics() async {
    final _localAuth = LocalAuthentication();
    bool _canCheckBiometrics = await _localAuth.canCheckBiometrics;

    if (_canCheckBiometrics) {
      List<BiometricType> _availableBiometrics =
          await _localAuth.getAvailableBiometrics();

        if (_availableBiometrics.contains(BiometricType.face)) {
          return AvailableBiometrics.face;
        }

        if (_availableBiometrics.contains(BiometricType.fingerprint)) {
          return AvailableBiometrics.fingerprint;
        }
    }

    return AvailableBiometrics.none;
  }

  Future<BiometricsStatus> authenticateWithBiometrics() async {
    final _localAuth = LocalAuthentication();
    final _canCheckBiometrics = await _localAuth.canCheckBiometrics;

    if (_canCheckBiometrics) {
      try {
        bool _didAuthenticate = await _localAuth.authenticate(
            localizedReason: 'Please authenticate to login',
            useErrorDialogs: true,
            stickyAuth: true,
            biometricOnly: true);

        if (_didAuthenticate) {
          return BiometricsStatus.authenticated;
        }

        return BiometricsStatus.denied;
      } on PlatformException catch (error) {
        if (error.code == auth_error.notAvailable) {
          print('error.code == auth_error.notAvailable');
        } else {
          print(error);
        }

        return BiometricsStatus.needAction;
      }
    }

    return BiometricsStatus.needAction;
  }
}
