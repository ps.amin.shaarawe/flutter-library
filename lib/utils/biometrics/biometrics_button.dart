import 'dart:io';
import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';
import 'biometrics_manager.dart';

typedef void BiometricsButtonCompletionHandler(BiometricsStatus authStatus);

class BiometricsButton extends StatefulWidget {
  final String title;
  final String fingerPrintAssetImage;
  final String faceIdPrintAssetImage;

  final Color iconColor;
  final Color titleColor;

  final double imageHeight;

  final BiometricsButtonCompletionHandler completionHandler;

  const BiometricsButton({
    Key key,
    this.title = '',
    @required this.completionHandler,
    this.fingerPrintAssetImage = 'assets/fingerprint.png',
    this.faceIdPrintAssetImage = 'assets/faceid.png',
    this.iconColor,
    this.titleColor,
    this.imageHeight = 50,
  }) : super(key: key);

  @override
  _BiometricsButtonState createState() => _BiometricsButtonState();
}

class _BiometricsButtonState extends State<BiometricsButton> {
  AvailableBiometrics _availableBiometrics;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.hasData) return snapshot.data;
        return Container();
      },
      future: _authWidget(),
    );
  }

  Future<Widget> _authWidget() async {
    _availableBiometrics =
        await BiometricsManager.shared.getAvailableBiometrics();

    final _token =
        await SecureStorageHelper().read(key: CacheKeys.biometricsLoginToken);

    if (_token.isEmpty) return Container();

    if (Platform.isIOS) return await _authIOSWidget();

    return await _authAndroidWidget();
  }

  Future<Widget> _authIOSWidget() async {
    return Container(
      width: MediaQuery.of(context).size.width * 0.8,
      child: Column(
        children: [
          InkWell(
            onTap: _startLocalAuth,
            child: Container(
              height: widget.imageHeight,
              child: Image.asset(
                _availableBiometrics == AvailableBiometrics.face
                    ? widget.faceIdPrintAssetImage
                    : widget.fingerPrintAssetImage,
                color: widget.iconColor == null
                    ? Theme.of(context).primaryColor
                    : widget.iconColor,
              ),
            ),
          ),
          _lblLoginBiometrics(),
        ],
      ),
    );
  }

  Future<Widget> _authAndroidWidget() async {
    return Container(
      width: MediaQuery.of(context).size.width * 0.8,
      child: Column(
        children: [
          InkWell(
            onTap: _startLocalAuth,
            child: Image.asset(
              widget.fingerPrintAssetImage,
              color: widget.iconColor == null
                  ? Theme.of(context).primaryColor
                  : widget.iconColor,
              scale: 3.5,
            ),
          ),
          _lblLoginBiometrics(),
        ],
      ),
    );
  }

  Widget _lblLoginBiometrics() {
    return Column(
      children: [
        SizedBox(height: 10),
        Text(widget.title.toString(),
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.035,
                color: widget.titleColor == null
                    ? Theme.of(context).primaryColor
                    : widget.titleColor)),
      ],
    );
  }

  void _startLocalAuth() async {
    final _authStatus =
        await BiometricsManager.shared.authenticateWithBiometrics();

    if (widget.completionHandler == null)
      throw Exception('BiometricsButton completionHandler should not be null');

    widget.completionHandler(_authStatus);
  }
}
