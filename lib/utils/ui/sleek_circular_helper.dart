import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/size_config.dart';
import 'package:mpay_flutter_library/utils/ui/step_view.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'common_views.dart';
import 'keyboard_Actions.dart';
import 'navigator_footer.dart';
class SleekCircularHelper {
  Widget getSleekCircularSlider(
    BuildContext context,
    FocusNode focusNode,
    TextEditingController controller, {
    Color trackColor = Colors.black12,
    Color shadowColor,
    Color dotColor = Colors.white,
    Color progressBarColor = Colors.white,
    List<Color> progressBarColors,
    double initValue = 10,
    void onChange(String value),
    bool readOnly = false,
    void Function() onEditingComplete,
    Color innerLabelColor = const Color(0xffA3A0A0),
    void Function() noteOnTap,
    double max,
    void Function() onNavigatorFooterNextTap,
    bool navigatorFooterIsPadding = false,
    bool navigatorFooterIsCenter = true,
    List<StepViewModel> stepsList,
    Widget extraFields,
    FocusNode extraFieldsNode,
    @required String currency,
    @required String nextTitle,
    @required Gradient globalGradient,
    @required Gradient accentGradient,
    @required List<TextInputFormatter> inputFormatters,
    @required EdgeInsetsGeometry stepViewPadding,
    @required String noteTitle,
    @required String title,
    @required String navigatorFooterNextTitle,
  }) {
    if (progressBarColors == null) {
      progressBarColors = accentGradient.colors;
    }
    if (shadowColor == null) {
      shadowColor = Theme.of(context).primaryColor;
    }

    if (stepsList == null) {
      stepsList = [];
    }

    var arrNodes = [focusNode];
    if (extraFieldsNode != null) {
      arrNodes.insert(0, extraFieldsNode);
    }

    return KeyboardActions(
      config: KeyBoardInputActions(
        context,
        arrNodes,
        PsLibrarySetupObject.shared.doneButtonTitle,
        nextTitle,
      ).buildConfig(),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _getStepView(stepsList, stepViewPadding),
              extraFields != null ? extraFields : Container(),
              CommonViews().hintTextView(title),
              AbsorbPointer(
                absorbing: readOnly,
                child: getSleekBar(
                  context,
                  controller,
                  focusNode,
                  trackColor,
                  shadowColor,
                  dotColor,
                  progressBarColor,
                  progressBarColors,
                  initValue,
                  max != null ? max : 500,
                  onChange,
                  readOnly,
                  onEditingComplete,
                  innerLabelColor,
                  PsLibrarySetupObject.shared.amountRex,
                  PsLibrarySetupObject.shared.isArabic,
                  currency,
                  inputFormatters,
                ),
              ),
              Container(height: 25),
              NavigatorFooter(
                title: navigatorFooterNextTitle,
                context: context,
                isPadding: navigatorFooterIsPadding,
                isCenter: navigatorFooterIsCenter,
                onNextTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  SystemChannels.textInput.invokeMethod('TextInput.hide');

                  if (onNavigatorFooterNextTap != null)
                    onNavigatorFooterNextTap();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getSleekBar(
    BuildContext context,
    TextEditingController controller,
    FocusNode focusNode,
    Color trackColor,
    Color shadowColor,
    Color dotColor,
    Color progressBarColor,
    List<Color> progressBarColors,
    double initValue,
    double max,
    void onChange(String value),
    bool readOnly,
    void Function() onEditingComplete,
    Color innerLabelColor,
    String amountRex,
    bool isArabic,
    String currency,
    List<TextInputFormatter> inputFormatters,
  ) {
    double _currencyWidth = 130;
    var numberFormat = NumberFormat("###0.00#", "en_US");





    return Stack(
      children: <Widget>[
        Center(
          child: SleekCircularSlider(
            appearance: CircularSliderAppearance(
                customColors: CustomSliderColors(
                    dotColor: dotColor,
                    progressBarColor: progressBarColor,
                    progressBarColors: progressBarColors,
                    trackColor: trackColor,
                    shadowColor: shadowColor),
                size: MediaQuery.of(context).size.width * 0.6,
                infoProperties: InfoProperties(
                  modifier: (double value) {
                    return '';
                  },
                ),
                customWidths: CustomSliderWidths(
                  trackWidth: 20,
                  progressBarWidth: 15,
                  handlerSize: 5,
                ),
                startAngle: 270,
                angleRange: 360),
            min: 0,
            max: max,
            initialValue: initValue,
            onChange: (value) {
              focusNode.unfocus();
              onChange(value.toInt().toString());
            },
          ),
        ),
        Positioned(
          top: 65,
          left: MediaQuery.of(context).size.width / 2 - 75,
          child: SizedBox(
            height: 130,
            child: TextField(
              focusNode: focusNode,
              readOnly: readOnly,
              onEditingComplete: onEditingComplete,
              autofocus: false,
              enabled: true,
              maxLines: 1,
              onSubmitted: (value) {
                FocusScope.of(context).requestFocus(new FocusNode());
                SystemChannels.textInput.invokeMethod('TextInput.hide');
              },
              inputFormatters: inputFormatters,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                  hintText: numberFormat.format(double.parse('0.0')),
                  contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  fillColor: Colors.red,
                  alignLabelWithHint: false),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockHorizontal * 8,
                fontWeight: FontWeight.bold,
              ),
              controller: controller,
            ),
          ),
          width: 150,
        ),
        Positioned(
          top: MediaQuery.of(context).size.width * 0.38,
          left: MediaQuery.of(context).size.width / 2 - (_currencyWidth / 2),
          child: Container(
            width: _currencyWidth,
            child: Center(
              child: Text(
                currency.toString(),
                style: TextStyle(
                    fontSize: SizeConfig.safeBlockHorizontal * 5,
                    color: innerLabelColor),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getStepView(
    List<StepViewModel> stepsList,
    EdgeInsetsGeometry stepViewPadding,
  ) {
    return Padding(
      padding: stepViewPadding,
      child: StepView(stepsList: stepsList),
    );
  }
}
