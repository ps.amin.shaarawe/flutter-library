import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';

class ProgressHud {
  static final ProgressHud shared = ProgressHud();
  BuildContext context;

  Widget createLoadingView() {
    return Stack(children: <Widget>[getCircularProgressIndicator()]);
  }

  Widget getCircularProgressIndicator({
    Color color = Colors.white,
    double size = 45.0,
  }) {
    if (PsLibrarySetupObject.shared.customSpinKit != null)
      return Center(child: PsLibrarySetupObject.shared.customSpinKit);

    return Center(child: SpinKitCubeGrid(color: color, size: size));
  }

  void startLoading(BuildContext context) {
    ProgressHud.shared.context = context;

    showDialog(
      barrierDismissible: false,
      context: ProgressHud.shared.context,
      builder: (context) {
        return createLoadingView();
      },
    );
  }

  void stopLoading() {
    if (ProgressHud.shared.context == null) {
      return;
    }

    try {
      Navigator.of(ProgressHud.shared.context, rootNavigator: true)
          .pop('Discard');
    } catch (e) {
      print(e.toString());
    }
  }
}
