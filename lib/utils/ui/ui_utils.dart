import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/utils/helpers/size_config.dart';

class UiUtiles {
  static var shared = UiUtiles();

  static double fontSizeDensity(BuildContext context, double dp) {
    return ((dp / 300) * MediaQuery.of(context).size.width);
  }

  Widget getWrappedColumn({
    List<Widget> children,
    double spacing = 0,
    double runSpacing = 0,
  }) {
    return Wrap(
      children: children == null ? [] : children,
      spacing: spacing,
      runSpacing: runSpacing,
    );
  }

  Widget getPaddedText({
    String title,
    TextStyle style,
    double padding = 20,
    int maxLines = 5,
  }) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: Container(
        height: 37,
        child: _getText(
          title: title,
          style: style,
          maxLines: maxLines,
        ),
      ),
    );
  }

  Text _getText({String title, TextStyle style, int maxLines = 5}) {
    if (title == null) {
      title = 'null';
    }
    return Text(
      title,
      style: style,
      overflow: TextOverflow.ellipsis,
      maxLines: maxLines,
      softWrap: true,
    );
  }

  TextStyle amounTextStyle({
    Color color = Colors.black38,
    FontWeight fontWeight = FontWeight.bold,
    double fontSize = 18,
  }) {
    return TextStyle(
      color: color,
      fontWeight: FontWeight.bold,
      fontSize: fontSize,
    );
  }

  TextStyle publicTextStyle({
    Color color = Colors.black38,
    FontWeight fontWeight = FontWeight.bold,
    double fontSize = -1,
    double height = 1.3,
  }) {
    if (fontSize == -1) {
      fontSize = SizeConfig.safeBlockHorizontal * 6;
    }
    return TextStyle(
      fontSize: fontSize,
      color: color,
      height: height,
      fontWeight: fontWeight,
    );
  }
}
