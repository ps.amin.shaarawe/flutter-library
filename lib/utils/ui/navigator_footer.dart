import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/gradient_border_container.dart';

class NavigatorFooter extends StatefulWidget {
  final void Function() onNextTap;
  final bool isActive;
  final bool isPadding;
  final bool isChecked;
  final BuildContext context;
  final String title;
  final bool isCenter;

  const NavigatorFooter({
    @required this.title,
    Key key,
    this.onNextTap,
    this.isActive = true,
    this.isPadding = true,
    this.isChecked = false,
    this.isCenter = true,
    this.context,
  }) : super(key: key);

  @override
  _NavigatorFooterState createState() => _NavigatorFooterState();
}

class _NavigatorFooterState extends State<NavigatorFooter> {
  final _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: _getNextButton(_width),
    );
  }

  Widget _getNextButton(_width) {
    return GestureDetector(
      onTap: widget.isActive
          ? widget.onNextTap
          : () {
              _focusNode.requestFocus(new FocusNode());
            },
      child: Padding(
        padding: widget.isPadding
            ? EdgeInsets.fromLTRB(
                _width < PsLibrarySetupObject.shared.minWidthForFontModification
                    ? 10
                    : 20,
                20,
                _width < PsLibrarySetupObject.shared.minWidthForFontModification
                    ? 10
                    : 20,
                10)
            : EdgeInsets.zero,
        child: Align(
          alignment: widget.isCenter
              ? Alignment.center
              : PsLibrarySetupObject.shared.isArabic
                  ? Alignment.bottomLeft
                  : Alignment.bottomRight,
          child: SizedBox(
            width: _width * 0.3,
            child: GradientBorderContainer(
              onPressed: widget.isActive
                  ? widget.onNextTap
                  : () {
                      _focusNode.requestFocus(new FocusNode());
                    },
              child: Text(
                widget.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: _width * 0.04),
              ),
              gradient: PsLibrarySetupObject.shared.globalGradient,
            ),
          ),
        ),
      ),
    );
  }
}
