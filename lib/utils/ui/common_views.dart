import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/gradient_border_container.dart';
import 'package:mpay_flutter_library/utils/ui/textfield_helper.dart';

class CommonViews {
  Widget searchField({
    @required BuildContext context,
    @required Function search,
    @required TextEditingController searchController,
    @required FocusNode node,
    @required String searchHint,
    @required EdgeInsetsGeometry textFieldPadding,
    @required int configDelayDuration,
  }) =>
      Padding(
        padding: textFieldPadding,
        child: TextFieldHelper.shared.createGradientField(
            context: context,
            inputAction: TextInputAction.done,
            controller: searchController,
            focusNode: node,
            onChanged: (value) {
              search(value);
            },
            onSubmitted: (val) {
              FocusScope.of(context).requestFocus(new FocusNode());
              SystemChannels.textInput.invokeMethod('TextInput.hide');
            },
            labelText: searchHint,
            prefixIcon: Icon(Icons.search, color: Colors.white),
            inputFormatters: [
              //FilteringTextInputFormatter(new RegExp(' '), allow: false),
            ]),
      );

  Widget gradiantLine({
    double width = 200,
    double height = 2,
    @required Gradient gradient,
  }) =>
      SizedBox(
        width: width,
        height: height,
        child: Container(decoration: BoxDecoration(gradient: gradient)),
      );

  Widget menuActionBtn({
    @required BuildContext context,
    @required String img,
    @required String title,
    @required String desc,
    void Function() onTap,
  }) {
    double imgWidth = 30;
    double imgHeight = 30;
    var _width = MediaQuery.of(context).size.width;
    var _allPading = EdgeInsets.fromLTRB(_width * 0.05, 5, _width * 0.05, 10);
    var _height = MediaQuery.of(context).size.height;
    return Padding(
      padding: _allPading,
      child: GestureDetector(
        onTap: onTap,
        child: GradientBorderContainer(
          onPressed: onTap,
          strokeWidth: 0.3,
          gradient: PsLibrarySetupObject.shared.accentGradient,
          child: Center(
            child: Container(
              alignment: Alignment.bottomRight,
              height: _height * 0.07,
              width: _width,
              child: Row(
                children: <Widget>[
                  Container(
                    child: Center(
                      child: Image.asset(
                        img,
                        width: imgWidth,
                        height: imgHeight,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          title,
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: _width * 0.04,
                              height: 1,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top:
                              PsLibrarySetupObject.shared.isArabic ? 1 : 0),
                          child: SizedBox(
                            width: _width * 0.7 ,
                            child: Text(
                              desc.toString(),
                              maxLines: 2,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Theme.of(context)
                                    .primaryColor
                                    .withAlpha(200),
                                fontSize: _width * 0.03,
                                height: 1.5,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget hintTextView(
      String title, {
        bool isCenter = true,
        TextStyle style =
        const TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
        EdgeInsetsGeometry padding = const EdgeInsets.fromLTRB(0, 0, 0, 20),
      }) {
    return Padding(
      padding: padding,
      child: Text(
        title.toString(),
        textAlign: isCenter ? TextAlign.center : TextAlign.justify,
        style: style,
      ),
    );
  }
}
