import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';

class KeyBoardInputActions {
  List<KeyboardActionsItem> actions = [];

  KeyBoardInputActions(
    BuildContext context,
    List<FocusNode> focusNodes,
    String doneButtonTitle,
    String nextTitle,
  ) {
    for (int counter = 0; counter < focusNodes.length; counter++) {
      actions.add(
        KeyboardActionsItem(focusNode: focusNodes[counter], toolbarButtons: [
          (node) {
            return GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                node.unfocus();
                // if (counter == focusNodes.length - 1) {
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                // } else {
                //   FocusScope.of(context).unfocus();
                //   Timer(Duration(milliseconds: 200), () {
                //     FocusScope.of(context)
                //         .requestFocus(focusNodes[counter + 1]);
                //   });
                // }
              },
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: Text(
                      // (counter == focusNodes.length - 1)
                      doneButtonTitle
                      // : nextTitle,
                      ),
                ),
              ),
            );
          }
        ]),
      );
    }
  }

  KeyboardActionsConfig buildConfig() {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: actions,
    );
  }
}
