import 'package:flutter/material.dart';

class StepViewModel {
  bool isDone;
  bool isCurrent;
  double lineWidth;
  double lineHeight;
  double stepWidth;
  double stepHeight;
  double stepRadius;
  Color color;
  Color textAndIconsColors;

  StepViewModel(
    this.isDone,
    this.isCurrent, {
    this.lineWidth = 25,
    this.lineHeight = 2,
    this.stepWidth = 40,
    this.stepHeight = 30,
    this.stepRadius = 20,
    this.color = const Color(0xFFE60037),
    this.textAndIconsColors = Colors.white,
  });
}

class StepView extends StatelessWidget {
  final List<StepViewModel> stepsList;

  const StepView({Key key, this.stepsList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: _getSteps(context),
    );
  }

  List<Widget> _getSteps(BuildContext context) {
    List<Widget> _stepsViews = [];

    for (int counter = 0; counter < stepsList.length; counter++) {
      final model = stepsList[counter];

      int _step = counter + 1;
      if (model.isDone) {
        _stepsViews.add(
          ClipRRect(
            borderRadius: BorderRadius.circular(model.stepRadius),
            child: Container(
              width: model.stepWidth,
              height: model.stepHeight,
              color: model.color,
              child: Center(
                child: Icon(
                  Icons.check,
                  color: model.textAndIconsColors,
                ),
              ),
            ),
          ),
        );
      } else if (model.isCurrent) {
        _stepsViews.add(
          ClipRRect(
            borderRadius: BorderRadius.circular(model.stepRadius),
            child: Container(
              width: model.stepWidth,
              height: model.stepHeight,
              color: model.color,
              child: Center(
                child: Text('$_step',
                    style: TextStyle(color: model.textAndIconsColors)),
              ),
            ),
          ),
        );
      } else {
        _stepsViews.add(SizedBox(
          width: model.stepWidth,
          height: model.stepHeight,
          child: MaterialButton(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(model.stepRadius),
                side: BorderSide(color: model.color)),
            child: Center(
              child: Text('$_step', style: TextStyle(color: model.color)),
            ),
            onPressed: () {},
          ),
        ));
      }

      if (counter < stepsList.length - 1) {
        if (model.isDone) {
          _stepsViews.add(
            ClipRect(
              child: Container(
                width: model.lineWidth,
                height: model.lineHeight,
                color: model.color,
              ),
            ),
          );
        } else {
          _stepsViews.add(
            ClipRect(
              child: Container(
                width: model.lineWidth,
                height: model.lineHeight,
                color: Colors.grey.withAlpha(80),
              ),
            ),
          );
        }
      }
    }

    return _stepsViews;
  }
}
