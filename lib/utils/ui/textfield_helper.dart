import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';
import 'package:mpay_flutter_library/utils/helpers/gradient_border_container.dart';
import 'package:mpay_flutter_library/utils/helpers/gradient_mask.dart';
import 'package:mpay_flutter_library/utils/ui/pin_code_text_field.dart';
import 'package:select_form_field/select_form_field.dart';

class TextFieldHelper {
  static var shared = TextFieldHelper();

  TextStyle getTextStyle({double height = 1}) => new TextStyle(
        fontSize: 15,
        height: height,
        color: Colors.black,
      );

  Widget createOtpTextField({
    Size size,
    TextEditingController controller,
    BuildContext context,
    FocusNode focusNode,
    void Function(String) onDone,
    dynamic Function(String) onTextChanged,
    bool hasError,
    bool obscureText = true,
    String maskCharacter = '•',
    bool hideCharacter = true,
    bool autofocus = true,
    bool highlight = true,
    double fontSize = 28,
    @required int otpLength,
  }) {
    return PinCodeTextField(
      maskCharacter: maskCharacter,
      hideCharacter: hideCharacter,
      controller: controller,
      hasTextBorderColor: Colors.black,
      focusNode: focusNode != null ? focusNode : new FocusNode(),
      maxLength: otpLength,
      defaultBorderColor: Theme.of(context).primaryColor,
      hasError: false,
      autofocus: true,
      onTextChanged: onTextChanged,
      onDone: onDone == null ? (value) {} : onDone,
      pinCodeTextFieldLayoutType: PinCodeTextFieldLayoutType.AUTO_ADJUST_WIDTH,
      pinTextStyle: TextStyle(fontSize: fontSize),
      pinTextAnimatedSwitcherTransition:
          ProvidedPinBoxTextAnimation.scalingTransition,
      pinTextAnimatedSwitcherDuration: Duration(milliseconds: 200),
      pinBoxWidth: 60,
      pinBoxHeight: 60,
    );
  }

  Widget createPinCodeTextField({
    Size size,
    TextEditingController controller,
    BuildContext context,
    void Function(String) onDone,
    dynamic Function(String) onTextChanged,
    bool hasError,
    bool obscureText = true,
    String maskCharacter = '•',
    FocusNode focusNode,
    bool hideCharacter = true,
    bool autofocus = true,
    bool highlight = true,
    double fontSize = 28,
    @required int configPinBoxesCount,
  }) {
    return new PinCodeTextField(
      inputFormatters: [],
      maskCharacter: maskCharacter,
      hideCharacter: hideCharacter,
      controller: controller,
      hasTextBorderColor: Colors.black,
      maxLength: configPinBoxesCount,
      defaultBorderColor: Theme.of(context).primaryColor,
      hasError: false,
      autofocus: true,
      onTextChanged: (value) {
        if (onTextChanged != null) onTextChanged(value);
      },
      onDone: onDone == null ? (value) {} : onDone,
      focusNode: focusNode != null ? focusNode : new FocusNode(),
      pinCodeTextFieldLayoutType: PinCodeTextFieldLayoutType.AUTO_ADJUST_WIDTH,
      pinTextStyle: TextStyle(fontSize: fontSize),
      pinTextAnimatedSwitcherTransition:
          ProvidedPinBoxTextAnimation.scalingTransition,
      pinTextAnimatedSwitcherDuration: Duration(milliseconds: 200),
      pinBoxWidth: 60,
      pinBoxHeight: 60,
    );
  }

  Widget createGradientField({
    TextInputType keyboardType = TextInputType.text,
    TextEditingController controller,
    String errorText = '',
    String labelText = '',
    String hintText = '',
    IconData icon = Icons.filter_none,
    String prefixText = '',
    void Function(String) onChanged,
    void Function() onTap,
    void Function() onEditingComplete,
    void Function(String s) onSubmitted,
    FocusNode focusNode,
    bool obscureText = false,
    bool enabled = true,
    List<FocusNode> allFocusNode,
    ImageIcon prefixIconData,
    ImageIcon suffixIconData,
    List<TextInputFormatter> inputFormatters,
    Widget prefixIcon,
    Widget suffixIcon,
    Gradient borderGradientColor,
    Gradient iconPrefixGradientColor,
    Gradient iconSuffixGradientColor,
    InputBorder inputBorder = InputBorder.none,
    void Function() onGradientPressed,
    TextInputType textInputType = TextInputType.text,
    TextInputAction inputAction = TextInputAction.done,
    BuildContext context,
    String hint,
    bool enableSuggestions = false,
    double strokeWidth = 1,
    bool autocorrect = false,
    TextCapitalization textCapitalization = TextCapitalization.none,
    EdgeInsetsGeometry contentPadding,
    FloatingLabelBehavior floatingLabelBehavior = FloatingLabelBehavior.auto,
    double height = 1,
  }) {
    if (borderGradientColor == null) {
      borderGradientColor = PsLibrarySetupObject.shared.globalGradient;
    }

    if (iconPrefixGradientColor == null) {
      iconPrefixGradientColor = PsLibrarySetupObject.shared.accentGradient;
    }

    if (iconSuffixGradientColor == null) {
      iconSuffixGradientColor = PsLibrarySetupObject.shared.accentGradient;
    }

    return Container(
      child: Column(
        children: <Widget>[
          GradientBorderContainer(
            padding: 0,
            gradient: borderGradientColor,
            onPressed: onGradientPressed,
            strokeWidth: strokeWidth,
            child: TextField(
              enableInteractiveSelection:
                  SharedParam.shared.isDebug || SharedParam.shared.isDemo,
              textCapitalization: textCapitalization,
              autocorrect: autocorrect,
              enableSuggestions: enableSuggestions,
              cursorColor: Theme.of(context).primaryColor,
              keyboardType: keyboardType,
              textInputAction: inputAction,
              controller: controller,
              autofocus: false,
              focusNode: focusNode != null ? focusNode : new FocusNode(),
              onChanged: onChanged,
              onSubmitted: (val) {
                if (onSubmitted != null)
                  Timer(
                      Duration(
                          milliseconds: PsLibrarySetupObject
                              .shared.configDelayDuration), () {
                    onSubmitted(val);
                  });
              },
              enabled: enabled,
              decoration: InputDecoration(
                floatingLabelBehavior: floatingLabelBehavior,
                contentPadding: contentPadding,
                prefixText: PsLibrarySetupObject.shared.isArabic
                    ? ''
                    : prefixText + ' ',
                suffixText: PsLibrarySetupObject.shared.isArabic
                    ? prefixText + ' '
                    : '',
                labelText: labelText,
                hintText: hintText,
                errorMaxLines: 3,
                prefixStyle: getTextStyle(),
                prefixIcon: Padding(
                  padding: EdgeInsets.all(0),
                  child: iconPrefixGradientColor != null
                      ? GradientMask(
                          gradient: iconPrefixGradientColor,
                          child: prefixIconData != null
                              ? prefixIconData
                              : prefixIcon,
                        )
                      : prefixIconData != null
                          ? prefixIconData
                          : prefixIcon,
                ),
                suffixIcon: Padding(
                  padding: const EdgeInsets.all(0),
                  child: iconSuffixGradientColor != null
                      ? suffixIconData != null
                          ? suffixIconData
                          : suffixIcon
                      : suffixIconData != null
                          ? suffixIconData
                          : suffixIcon,
                ),
                border: inputBorder,
              ),
              obscureText: obscureText,
              style: getTextStyle(height: height),
              inputFormatters: inputFormatters,
              onTap: () {
                if (onTap != null) {
                  onTap();
                }
              },
              onEditingComplete: () {
                FocusScope.of(context).requestFocus(new FocusNode());
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                if (onEditingComplete != null) {
                  onEditingComplete();
                }
              },
            ),
          ),
          if (errorText != null && errorText.isNotEmpty)
            Align(
              alignment: PsLibrarySetupObject.shared.isArabic
                  ? Alignment.centerRight
                  : Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(top: 0, left: 10, right: 10),
                child: Text(
                  errorText,
                  style: TextStyle(color: Colors.blueAccent, fontSize: 11),
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget createDropDown({
    TextInputType keyboardType = TextInputType.text,
    TextEditingController controller,
    SelectFormFieldType dropDownType = SelectFormFieldType.dropdown,
    String errorText = '',
    String labelText = '',
    String hintText = '',
    String dialogTitle = '',
    String dialogSearchHint = '',
    bool enableSearch = false,
    IconData icon = Icons.filter_none,
    String prefixText = '',
    void Function(String) onChanged,
    void Function(String) onSaved,
    List<Map<String, dynamic>> items,
    void Function() onTap,
    void Function() onEditingComplete,
    void Function(String s) onSubmitted,
    FocusNode focusNode,
    bool obscureText = false,
    bool enabled = true,
    List<FocusNode> allFocusNode,
    ImageIcon prefixIconData,
    ImageIcon suffixIconData,
    List<TextInputFormatter> inputFormatters,
    Widget prefixIcon,
    Widget suffixIcon,
    Gradient borderGradient,
    Gradient iconPrefixGradient,
    Gradient iconSuffixGradient,
    InputBorder inputBorder,
    void Function() onGradientPressed,
    TextInputType textInputType = TextInputType.text,
    TextInputAction inputAction = TextInputAction.done,
    BuildContext context,
    String hint,
    bool enableSuggestions = false,
    double strokeWidth = 2,
    bool autocorrect = false,
    TextCapitalization textCapitalization = TextCapitalization.none,
    EdgeInsetsGeometry contentPadding,
    FloatingLabelBehavior floatingLabelBehavior = FloatingLabelBehavior.auto,
    double height = 1,
  }) {
    if (borderGradient == null) {
      borderGradient = PsLibrarySetupObject.shared.globalGradient;
    }
    if (inputBorder == null) {
      inputBorder = OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 1.8),
          borderRadius: BorderRadius.circular(40));
    }

    if (iconPrefixGradient == null) {
      iconPrefixGradient = PsLibrarySetupObject.shared.globalGradient;
    }

    if (iconSuffixGradient == null) {
      iconSuffixGradient = PsLibrarySetupObject.shared.globalGradient;
    }
    return SelectFormField(
      type: dropDownType,
      icon: Icon(icon),
      labelText: 'Shape',
      items: items,
      strutStyle: StrutStyle(height: 1),
      textCapitalization: textCapitalization,
      autocorrect: autocorrect,
      enableSuggestions: enableSuggestions,
      cursorColor: Theme.of(context).primaryColor,
      keyboardType: keyboardType,
      textInputAction: inputAction,
      controller: controller,
      autofocus: false,
      style: getTextStyle(height: height),
      focusNode: focusNode != null ? focusNode : new FocusNode(),
      onChanged: onChanged,
      enabled: enabled,
      decoration: InputDecoration(
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
        floatingLabelBehavior: floatingLabelBehavior,
        contentPadding: contentPadding,
        prefixText:
            PsLibrarySetupObject.shared.isArabic ? '' : prefixText + ' ',
        suffixText:
            PsLibrarySetupObject.shared.isArabic ? prefixText + ' ' : '',
        labelText: labelText,
        // labelStyle: getTextStyle(),
        hintText: hintText,
        errorMaxLines: 3,
        errorText: errorText.isEmpty ? null : errorText,
        prefixStyle: getTextStyle(),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        border: inputBorder,
      ),
      onSaved: onSaved,
      dialogTitle: dialogTitle,
      dialogSearchHint: dialogSearchHint,
      enableSearch: enableSearch,
    );
  }

  Widget createTextField({
    TextInputType keyboardType = TextInputType.text,
    TextEditingController controller,
    Key key,
    String errorText = '',
    String labelText = '',
    String hintText = '',
    IconData icon = Icons.filter_none,
    String prefixText = '',
    void Function(String) onChanged,
    void Function() onTap,
    void Function() onEditingComplete,
    void Function(String s) onSubmitted,
    FocusNode focusNode,
    bool obscureText = false,
    bool enabled = true,
    bool isRead = false,
    List<FocusNode> allFocusNode,
    ImageIcon prefixIconData,
    ImageIcon suffixIconData,
    List<TextInputFormatter> inputFormatters,
    Widget prefixIcon,
    Widget suffixIcon,
    Gradient borderGradient,
    Gradient iconPrefixGradient,
    Gradient iconSuffixGradient,
    InputBorder inputBorder,
    void Function() onGradientPressed,
    TextInputType textInputType = TextInputType.text,
    TextInputAction inputAction = TextInputAction.done,
    BuildContext context,
    String hint,
    bool enableSuggestions = false,
    double strokeWidth = 2,
    bool autocorrect = false,
    TextCapitalization textCapitalization = TextCapitalization.none,
    EdgeInsetsGeometry contentPadding,
    FloatingLabelBehavior floatingLabelBehavior = FloatingLabelBehavior.auto,
    double height = 1,
  }) {
    if (borderGradient == null) {
      borderGradient = PsLibrarySetupObject.shared.globalGradient;
    }
    if (inputBorder == null) {
      inputBorder = OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey),
      );
    }

    if (iconPrefixGradient == null) {
      iconPrefixGradient = PsLibrarySetupObject.shared.globalGradient;
    }

    if (iconSuffixGradient == null) {
      iconSuffixGradient = PsLibrarySetupObject.shared.globalGradient;
    }

    return TextField(
      textCapitalization: textCapitalization,
      autocorrect: autocorrect,
      readOnly: isRead,
      enableSuggestions: enableSuggestions,
      cursorColor: Theme.of(context).primaryColor,
      keyboardType: keyboardType,
      textInputAction: inputAction,
      controller: controller,
      autofocus: false,
      focusNode: focusNode != null ? focusNode : new FocusNode(),
      onChanged: onChanged,
      onSubmitted: onSubmitted,
      enabled: enabled,
      decoration: InputDecoration(
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
        floatingLabelBehavior: floatingLabelBehavior,
        contentPadding: contentPadding,
        prefixText: PsLibrarySetupObject.shared.isArabic ? '' : prefixText + ' ',
        suffixText: PsLibrarySetupObject.shared.isArabic ? prefixText + ' ' : '',
        labelText: labelText,
        labelStyle: getTextStyle(),
        hintText: hintText,
        errorMaxLines: 3,
        errorText: errorText.isEmpty ? null : errorText,
        prefixStyle: getTextStyle(),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        border: inputBorder,
      ),
      obscureText: obscureText,
      style: getTextStyle(height: height),
      inputFormatters: inputFormatters,
      onTap: onTap,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        // SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (onEditingComplete != null) {
          onEditingComplete();
        }
      },
    );
  }

}

Widget getWidgetWithPadding(Widget textField,
    {EdgeInsetsGeometry padding = const EdgeInsets.fromLTRB(40, 10, 40, 0)}) {
  return Padding(padding: padding, child: textField);
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
