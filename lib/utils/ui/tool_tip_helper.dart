import 'package:super_tooltip/super_tooltip.dart';

class ToolTipHelper {
  SuperTooltip _tooltip;

  SuperTooltip get tooltip => _tooltip;

  set tooltip(SuperTooltip value) {
    if (isExistsAndOpen()) _tooltip.close();
    _tooltip = value;
  }

  bool isExistsAndOpen() => _tooltip != null && _tooltip.isOpen;

  static var shared = ToolTipHelper();
}
