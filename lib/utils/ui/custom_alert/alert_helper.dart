import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/url_helper.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/alert.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/dialog_button.dart';

typedef void AlertHelperCompletionHandler();
typedef void ConfirmationAlertHelperCompletionHandler(bool isPositive);
enum AlertType { WARNING, ERROR, SUCCESS, INFO }

class AlertHelper {
  static final shared = AlertHelper();

  BuildContext context;
  final borderRadius = 40.0;
  final buttonWidth = 65.0;
  final boldFontWeight = FontWeight.bold;

  Future<void> showCustomDialog({
    String submitLabel = '',
    String cancelButtonLabel = '',
    @required BuildContext privateContext,
    Widget body,
    Widget icon,
    AlertHelperCompletionHandler cancelCompletionHandler,
    AlertHelperCompletionHandler completionHandler,
  }) async {
    if (cancelButtonLabel.isEmpty) {
      cancelButtonLabel = PsLibrarySetupObject.shared.cancelButtonLabel;
    }
    return Alert(
      context: privateContext,
      title: icon,
      closeIcon:
          Icon(Icons.close, color: Theme.of(privateContext).primaryColor),
      closeFunction: () {
        Navigator.of(privateContext, rootNavigator: true).pop();
      },
      image: icon,
      onWillPopActive: true,
      type: null,
      buttons: [
        DialogButton(
          color: Theme.of(privateContext).primaryColor,
          onPressed: () {
            Navigator.of(privateContext, rootNavigator: true).pop();
          },
          child: Text(
            cancelButtonLabel.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: MediaQuery.of(privateContext).size.width * 0.04,
            ),
          ),
        ),
        DialogButton(
          onPressed: () {
            Navigator.of(privateContext, rootNavigator: true).pop();
            if (completionHandler != null) completionHandler.call();
          },
          color: Theme.of(privateContext).primaryColor,
          child: Text(
            submitLabel.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: MediaQuery.of(privateContext).size.width * 0.04,
            ),
          ),
        ),
      ],
      content: body,
    ).show();
  }

  void showAlertDialog({
    @required String message,
    String buttonLabel = '',
    String cancelButtonLabel = '',
    @required BuildContext context,
    AlertType alertType = AlertType.INFO,
    bool isCancelButton = false,
    AlertHelperCompletionHandler cancelCompletionHandler,
    bool isCallButton,
    AlertHelperCompletionHandler completionHandler,
    Widget customWidget,
  }) {
    if (message == null || context == null) {
      return;
    }

    this.context = context;

    if (cancelButtonLabel.isEmpty)
      cancelButtonLabel = PsLibrarySetupObject.shared.cancelButtonLabel;

    bool isCall = message.toLowerCase() ==
            PsLibrarySetupObject.shared.generalError.toLowerCase() &&
        PsLibrarySetupObject.shared.contactUsPhone
            .toString()
            .replaceAll('null', '')
            .isNotEmpty;

    if (isCallButton != null) isCall = isCallButton;

    Alert(
      context: AlertHelper.shared.context,
      title: Column(
        children: [
          Container(
            child: Text(
              message.toString(),
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.045,
                color: Colors.black,
              ),
            ),
          ),
          if (customWidget != null) customWidget,
        ],
      ),
      buttons: [
        if (isCall || isCancelButton)
          _getAlertButton(
            completionHandler:
                isCancelButton ? cancelCompletionHandler : completionHandler,
            label: isCancelButton
                ? cancelButtonLabel
                : PsLibrarySetupObject.shared.callTitle,
            isCancelButton: isCancelButton,
            isFirstButton: true,
          ),
        _getAlertButton(
          completionHandler: completionHandler,
          label: buttonLabel,
        ),
      ],
    ).show();
  }

  Widget _getAlertButton({
    @required AlertHelperCompletionHandler completionHandler,
    @required String label,
    bool isFirstButton = false,
    bool isCancelButton = false,
  }) =>
      DialogButton(
        border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        radius: BorderRadius.circular(40),
        color: Colors.transparent,
        child: new ConstrainedBox(
          constraints: new BoxConstraints(
            minWidth: MediaQuery.of(context).size.width * 0.2,
          ),
          child: Center(
            child: Text(
              label != null && label.isNotEmpty
                  ? label.toString()
                  : PsLibrarySetupObject.shared.okButtonTitle,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor,
                fontSize: MediaQuery.of(context).size.width * 0.03,
              ),
            ),
          ),
        ),
        onPressed: () {
          Navigator.of(context, rootNavigator: true).pop();
          if (isFirstButton) {
            if (isCancelButton) {
              if (completionHandler != null) completionHandler();
            } else {
              UrlHelper.shared.hitLink(
                  url: 'tel:' +
                      PsLibrarySetupObject.shared.contactUsPhone
                          .toString()
                          .replaceAll(' ', ''));
              if (completionHandler != null) {
                completionHandler();
              }
            }
          } else {
            if (completionHandler != null) {
              completionHandler();
            }
          }
        },
      );

  Future<void> showDatePickerAlertDialog({
    Widget title,
    String submitLabel = '',
    String cancelButtonLabel = '',
    @required BuildContext privateContext,
    Widget body,
    AlertType type,
    Widget icon,
    AlertHelperCompletionHandler cancelCompletionHandler,
    AlertHelperCompletionHandler completionHandler,
  }) async {
    if (cancelButtonLabel.isEmpty) {
      cancelButtonLabel = PsLibrarySetupObject.shared.cancelButtonLabel;
    }
    return Alert(
      context: privateContext,
      title: icon,
      closeIcon: Icon(Icons.close, color: Theme.of(privateContext).accentColor),
      closeFunction: () {
        Navigator.of(privateContext, rootNavigator: true).pop();
      },
      image: icon,
      onWillPopActive: true,
      buttons: [
        DialogButton(
          color: Theme.of(privateContext).accentColor,
          onPressed: () {
            Navigator.of(privateContext, rootNavigator: true).pop();
          },
          child: Text(
            cancelButtonLabel.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: MediaQuery.of(privateContext).size.width * 0.04,
            ),
          ),
        ),
        DialogButton(
          onPressed: () {
            Navigator.of(privateContext, rootNavigator: true).pop();
            if (completionHandler != null) completionHandler.call();
          },
          color: Theme.of(privateContext).accentColor,
          child: Text(
            submitLabel.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: MediaQuery.of(privateContext).size.width * 0.04,
            ),
          ),
        ),
      ],
      content: body,
    ).show();
  }
}
