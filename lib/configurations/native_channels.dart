class PsMpayFrameworkNativeChannels {
  static const mainChannel = "MAIN_CHANNEL";
  static const signText = "METHOD_SIGN_TEXT";
  static const getPublicKey = "METHOD_GET_PUBLIC_KEY";
  static const playBeepSound = "METHOD_PLAY_BEEP_SOUND";
  static const checkJailbreak = "METHOD_CHECK_JAILBREAK";
  static const encryptRsa = "METHOD_ENCRYPT_RSA";
  static const decryptRsa = "METHOD_DECRYPT_RSA";
  static const enableScreenShotPrevent = "METHOD_ENABLE_SCREEN_SHOT_PREVENT";
  static const localAuth = "METHOD_LOCAL_AUTH";
  static const localAuthValidation = "METHOD_LOCAL_AUTH_VALIDATION";
}
