class CacheKeys {
  static const systemConfigration = 'SYSTEM_CONFIGURATION';
  static const currentLanguage = 'CURRENT_LANGUAGE';
  static const isSkipIntroOld = 'IS_SKIP_INTRO';
  static const isSkipIntroUpdate = 'IS_SKIP_INTRO_V2';
  static const sessionId = 'SESSION_ID';
  static const savedBills = 'SAVED_BILLS';
  static const deviceId = 'DEVICE_ID';
  static const biometricsLoginToken = 'BIOMETRICS_LOGIN_TOKEN';
  static const mobileNumber = 'MOBILE_NUMBER';
  static const branchName = 'BRANCH_NAME';
  static const devicePublicKey = 'DEVICE_PUBLIC_KEY';
  static const devicePrivateKey = 'DEVICE_PRIVATE_KEY';
  static const serviceName = 'SERVICE_NAME';
  static const countryCode = 'COUNTRY_CODE';
  static const savedMobileNumber = 'SAVED_MOBILE_NUMBER';
  static const notificationLanguage = 'NOTIFICATION_LANGUAGE';
  static const isBiometricsEnabled = 'IS_BIOMETRICS_ENABLED';
  static const balanceVisible = 'BALANCE_VISIBLE';
  static const firstOpen = 'FIRST_OPEN';
}
