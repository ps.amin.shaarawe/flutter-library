class NotificationCenter {
  static String selectHome = 'SELECT_HOME';
  static String logout = 'LOGOUT';
  static String pushFullScreen = 'PUSH_FULL_SCREEN';
  static String pushNamedAndRemoveUntil = 'PUSH_NAMED_AND_REMOVE_UNTIL';
  static String pushAndRemoveUntil = 'PUSH_AND_REMOVE_UNTIL';
  static String moveIndex = 'MOVE_INDEX';
  static String loginScreen = 'LOGIN';
  static String showSettingsSuccess = 'SHOW_SETTINGS_SUCCESS';
}
