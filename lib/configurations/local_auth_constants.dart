class AndroidLocalAuthConstants {
  static const SUCCESS = '01';
  static const DEVICE_NOT_SUPPORT = '02';
  static const PERMISSION_NOT_ENABLED = '03';
  static const NO_FINGERPRINT_REGISTER = '04';
  static const NO_FINGERPRINT_ENABLE = '05';
  static const AUTH_FAILD = '06';
}

class IOSLocalAuthConstants {
  static const AUTH_SUCCESS = '01';
  static const FACE_ID_AVAILABLE = '02';
  static const TOUCH_ID_AVAILABLE = '03';
  static const DEVICE_NOT_SUPPORT = '04';
  static const NO_BIOMETRECS_ENABLE = '05';
  static const AUTH_FAILD = '06';
}
