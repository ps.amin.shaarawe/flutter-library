import 'package:easy_contact_picker/easy_contact_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ContactListInteractor {
  void getContacts({IOnContactListFinishedListener listener}) async {
    var permission = await Permission.contacts.request();

    if (permission == PermissionStatus.granted) {
      final EasyContactPicker _contactPicker = new EasyContactPicker();
      List<Contact> contacts = await _contactPicker.selectContacts();
      List<Contact> finalList = [];

      for (Contact contact in contacts) {
        var phoneNumber = contact.phoneNumber.replaceAll(' ', '');
        phoneNumber = phoneNumber.replaceAll(')', '');
        phoneNumber = phoneNumber.replaceAll('(', '');
        phoneNumber = phoneNumber.replaceAll('+', '');
        phoneNumber = phoneNumber.replaceAll('-', '');

        phoneNumber = _phoneNumberValidator(phoneNumber);
        if (phoneNumber != null) {
          var _contact = Contact(
              phoneNumber: phoneNumber,
              fullName: contact.fullName,
              firstLetter: contact.firstLetter);
          finalList.add(_contact);
        }
      }
      listener.getContactsDone(finalList);
    } else {
      listener.getContactsFaild();
    }
  }

  String _phoneNumberValidator(String value) {
    Pattern pattern = '[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(value))
      return null;
    else
      return value;
  }
}

abstract class IOnContactListFinishedListener {
  void getContactsDone(contacts);

  void getContactsFaild();
}
