import 'package:easy_contact_picker/easy_contact_picker.dart';
import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/size_config.dart';

class ContactListItem extends StatefulWidget {
  final Contact item;
  final Contact selectedContact;

  ContactListItem(this.item, this.selectedContact);

  @override
  _ContactListItemState createState() => _ContactListItemState();
}

class _ContactListItemState extends State<ContactListItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.only(bottom: 5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              _getCircle(),
              Padding(
                padding: EdgeInsets.only(
                    right: PsLibrarySetupObject.shared.isArabic ? 10 : 0),
                child: SizedBox(
                    child: Padding(
                        padding: EdgeInsets.only(top: 7),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.item.fullName.toString(),
                              style: TextStyle(
                                fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                            Text(
                              widget.item.phoneNumber.toString(),
                              style: _getTextStyle(),
                            ),
                          ],
                        ))),
              ),
            ],
          ),
          Divider(
            height: 2,
          ),
        ],
      ),
    );
  }

  Widget _getCircle() => Padding(
      padding: _getPadding(),
      child: Container(
        child: Center(
          child: Text(
            widget.item.firstLetter.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: SizeConfig.safeBlockHorizontal * 3,
            ),
          ),
        ),
        width: 36,
        height: 36,
        decoration: new BoxDecoration(
          color: Theme.of(context).accentColor,
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.transparent,
            width: 2,
          ),
        ),
      ));

  EdgeInsets _getPadding() => EdgeInsets.fromLTRB(0, 10, 8, 4);

  TextStyle _getTextStyle() => TextStyle(
      fontSize: SizeConfig.safeBlockHorizontal * 3.4,
      height: 1.3,
      color: Theme.of(context).accentColor);
}
