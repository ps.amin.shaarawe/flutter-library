import 'package:flutter/material.dart';

import 'contact_list_interactor.dart';
import 'contact_list_view.dart';

class ContactListPresenter implements IOnContactListFinishedListener {
  final IContactListView view;
  final BuildContext context;
  var interactor = new ContactListInteractor();

  ContactListPresenter({@required this.view, @required this.context});

  @override
  void getContactsDone(contacts) {
    view.getContactDone(contacts);
  }

  @override
  void getContactsFaild() {
    view.getContactFailed();
  }

  void getContacts() {
    interactor.getContacts(listener: this);
  }
}
