import 'dart:io';
import 'package:easy_contact_picker/easy_contact_picker.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/ui/common_views.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/alert_helper.dart';
import 'package:open_app_settings/open_app_settings.dart';
import 'contact_list_item.dart';
import 'contact_list_presenter.dart';
import 'contact_list_view.dart';

abstract class IContactListScreen {
  void didSelectRow(Contact contact);
}

class ContactListScreen extends StatefulWidget {
  final IContactListScreen completion;
  final Contact selectedContact;
  final EdgeInsetsGeometry textFieldPadding;
  final String searchHint;
  final String contactPermissionNeeded;
  final String openSettings;
  final String warning;

  ContactListScreen({
    Key key,
    @required this.completion,
    @required this.selectedContact,
    @required this.searchHint,
    @required this.contactPermissionNeeded,
    @required this.textFieldPadding,
    @required this.openSettings,
    @required this.warning,
  }) : super(key: key);

  _ContactListScreenState createState() => _ContactListScreenState();
}

class _ContactListScreenState extends State<ContactListScreen>
    implements IContactListView {
  Iterable<Contact> _mainArray = [];
  Iterable<Contact> _originalArray = [];
  ContactListPresenter _presenter;
  final _searchController = new TextEditingController();
  final _contactNode = new FocusNode();
  var _permissionDenied = false;

  @override
  void initState() {
    super.initState();
    _presenter = ContactListPresenter(view: this, context: context);
    _presenter.getContacts();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: _mainArray.isEmpty ? _noDataView() : _searchBar(),
      ),
    );
  }

  final kInnerDecoration = BoxDecoration(
    color: Colors.white,
    border: Border.all(color: Colors.white),
    borderRadius: BorderRadius.circular(32),
  );

  final kGradientBoxDecoration = BoxDecoration(
    gradient: PsLibrarySetupObject.shared.accentGradient,
    border: Border.all(
      color: Colors.white,
    ),
    borderRadius: BorderRadius.circular(32),
  );

  Widget _searchBar() {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          padding: EdgeInsets.all(8.0),
          child: CommonViews().searchField(
            context: context,
            search: _search,
            searchController: _searchController,
            node: _contactNode,
            configDelayDuration:
                PsLibrarySetupObject.shared.configDelayDuration,
            searchHint: widget.searchHint,
            textFieldPadding: widget.textFieldPadding,
          ),
        ),
        _listView(),
      ],
    );
  }

  Widget _noDataView() {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              widget.contactPermissionNeeded,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }

  Widget _listView() {
    return Expanded(
      // height: MediaQuery.of(context).size.height * 0.6,
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: false,
        padding: const EdgeInsets.all(8),
        itemCount: _mainArray.length,
        itemBuilder: (BuildContext context, int index) {
          var _item = _mainArray.toList()[index];
          return Container(
            child: Center(
              child: InkWell(
                onTap: () {
                  if (widget.completion != null) {
                    widget.completion.didSelectRow(_item);
                  }
                },
                child: ContactListItem(_item, widget.selectedContact),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void getContactDone(contacts) {
    setState(() {
      _mainArray = contacts;
      _originalArray = contacts;
    });
  }

  @override
  void getContactFailed() async {
    final permissionValidator = EasyPermissionValidator(
      goToSettingsText: widget.openSettings,
      context: context,
      appName: widget.warning,
    );

    _permissionDenied = await permissionValidator.contacts();
    setState(() {});

    if (Platform.isIOS && !_permissionDenied)
      AlertHelper.shared.showAlertDialog(
        message: widget.contactPermissionNeeded,
        context: context,
        isCancelButton: true,
        buttonLabel: widget.openSettings,
        cancelCompletionHandler: () {},
        completionHandler: () async {
          await OpenAppSettings.openSecuritySettings();
        },
      );
  }

  void _search(String value) {
    setState(() {
      _mainArray = _originalArray
          .where((object) =>
              object.fullName.toLowerCase().contains(value.toLowerCase()))
          .toList();

      if (_mainArray.length == 0) {
        _mainArray = _originalArray;
      }
    });
  }
}
