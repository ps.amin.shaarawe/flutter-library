import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/helpers/formate_helper.dart';
import 'package:mpay_flutter_library/utils/helpers/shared_prefrences_helper.dart';
import 'package:mpay_flutter_library/utils/helpers/size_config.dart';

class BalanceView extends StatefulWidget {
  const BalanceView({this.onBalanceVisibilityChanged, this.balance});

  final BalanceCallback onBalanceVisibilityChanged;
  final double balance;

  @override
  _BalanceViewState createState() => _BalanceViewState();
}

class _BalanceViewState extends State<BalanceView>
    with TickerProviderStateMixin {
  Animation<double> _animation;
  AnimationController _controller;
  bool _balanceVisible = true;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SharedPreferencesHelper()
        .readBoolean(key: CacheKeys.balanceVisible)
        .then((value) => _balanceVisible = value);

    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1500),
    );
    _animation = _controller.view;
  }

  @override
  Widget build(BuildContext context) {
    _animation = new Tween<double>(
      begin: _animation.value,
      end: this.widget.balance,
    ).animate(new CurvedAnimation(
      curve: Curves.easeInOutExpo,
      parent: _controller,
    ));
    _controller.forward(from: 0.0);

    var formattedAmount = FormateHelper.shared
        .getStringAmountFormatted(
            original: this.widget.balance.toString(),
            fractionDigits: PsLibrarySetupObject.shared.fractionDigits)
        .replaceAll(',', '');
    final fractionsList = formattedAmount.split('.');
    var fractions = '';
    var fractionsX = '';
    for (int i = 0; i < PsLibrarySetupObject.shared.fractionDigits; i++) {
      fractions = fractions + '0';
      fractionsX = fractionsX + 'x';
    }
    var realNumbers = '0';
    if (fractionsList.length > 1) {
      fractions = fractionsList[1];
      realNumbers = fractionsList[0];
    }
    var maskedBalance = '';
    for (int counter = 0; counter < realNumbers.length; counter++) {
      maskedBalance = maskedBalance + 'X';
    }
    return AnimatedBuilder(
        animation: _animation,
        builder: (BuildContext context, Widget child) {
          return Directionality(
              textDirection: TextDirection.ltr,
              child: Row(
                mainAxisAlignment: PsLibrarySetupObject.shared.isArabic
                    ? MainAxisAlignment.end
                    : MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    !_balanceVisible
                        ? maskedBalance + ' . '
                        : _animation.value.toInt().toString() + ' . ',
                    style: TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).primaryColor),
                  ),
                  Wrap(
                    direction: Axis.vertical,
                    spacing: MediaQuery.of(context).size.width * -0.04,
                    runSpacing: MediaQuery.of(context).size.width * -0.04,
                    alignment: WrapAlignment.start,
                    children: [
                      Text(
                        !_balanceVisible ? fractionsX : fractions,
                        style: TextStyle(
                            fontSize: SizeConfig.safeBlockHorizontal * 5,
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).accentColor),
                      ),
                      Text(
                        PsLibrarySetupObject.shared.currency,
                        style: TextStyle(
                            fontSize: SizeConfig.safeBlockHorizontal * 5,
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).accentColor),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5, top: 17, right: 5),
                    child: InkWell(
                      onTap: () {
                        _balanceVisible = !_balanceVisible;
                        SharedPreferencesHelper().saveBoolean(
                            key: CacheKeys.balanceVisible,
                            value: _balanceVisible);

                        setState(() {});
                        widget.onBalanceVisibilityChanged(_balanceVisible);
                      },
                      child: Padding(
                        padding: EdgeInsets.only(left: 5, right: 5),
                        child: SizedBox(
                            width: 25,
                            child: Icon(
                              _balanceVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColor,
                            )),
                      ),
                    ),
                  ),
                ],
              ));
        });
  }
}

typedef BalanceCallback = void Function(bool balanceValue);
