class SystemConfigurationObject {
  final String section;
  final String configKey;
  final String configValue;
  final String hint;

  SystemConfigurationObject(
      {this.section, this.configKey, this.configValue, this.hint});

  static List<SystemConfigurationObject> init(List json) {
    List<SystemConfigurationObject> parsedArray = [];

    for (Map<String, dynamic> object in json) {
      parsedArray.add(SystemConfigurationObject(
          section: object['section'],
          configKey: object['configKey'],
          configValue: object['configValue'],
          hint: object['hint']));
    }

    return parsedArray;
  }
}
