class SharedParam {
  static var shared = SharedParam();

  String baseUrl = '';
  String uploadUrl = '';

  /// If pinningCertificate is empty, then SSL pining will return true always
  String pinningCertificate = '';
  String tenant = '';
  String senderType = '';
  String sender = '';
  String sessionId = '';
  String lang = '';
  String deviceId = '';
  String languageId = '1';
  String splashRoute = '/splash';
  String deviceActivationRoute = '/deviceActivation';

  /// If checksum is empty, then checksum will not used
  String checksum = '';

  /// If checksumValue is empty, then checksum will not used always
  String checksumValue = '';

  bool isDebug = false;
  bool isDemo = false;

  SharedParam setup(
      {String baseUrl = '',
      String uploadUrl = '',
      String pinningCertificate = '',
      String tenant = '',
      String senderType = 'M',
      String deviceActivationRoute = '/deviceActivation',
      String lang = 'en',
      String splashRoute = '/splash',
      String deviceId = '',
      String checksum = '',
      String checksumValue = '',
      bool isDebug = false,
      bool isDemo = false}) {
    this.baseUrl = baseUrl;
    this.uploadUrl = uploadUrl;
    this.pinningCertificate = pinningCertificate;
    this.tenant = tenant;
    this.senderType = senderType;
    this.deviceActivationRoute = deviceActivationRoute;
    this.lang = lang;
    this.splashRoute = splashRoute;
    this.deviceId = deviceId;
    this.checksum = checksum;
    this.checksumValue = checksumValue;
    this.isDebug = isDebug;
    this.isDemo = isDemo;
    return this;
  }
}
