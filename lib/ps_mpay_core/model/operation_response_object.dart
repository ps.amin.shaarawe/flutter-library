import 'extra_data_object.dart';

class OperationResponseObject {
  ResponseObject response = ResponseObject();
  String token = '';

  OperationResponseObject({this.response, this.token});

  factory OperationResponseObject.fromJson(Map<String, dynamic> json) {
    return OperationResponseObject(
      response: ResponseObject.fromJson(json['response']),
      token: json['token'],
    );
  }
}

class ResponseObject {
  int ref = 0;
  String statusCode = '';
  String desc = '';
  String errorCd = '';
  List<ExtraDataObject> extraData = [];

  ResponseObject({
    this.ref,
    this.statusCode,
    this.desc,
    this.errorCd,
    this.extraData,
  });

  factory ResponseObject.fromJson(Map<String, dynamic> json) {
    return ResponseObject(
        ref: json['ref'],
        statusCode: json['statusCode'],
        desc: json['desc'],
        errorCd: json['errorCd'],
        extraData: ExtraDataObject().parseArray(json['extraData']));
  }
}
