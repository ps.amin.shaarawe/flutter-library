class ExtraDataObject {
  String key = '';
  String value = '';

  ExtraDataObject({this.key, this.value});

  List<ExtraDataObject> parseArray(List json) {
    List<ExtraDataObject> parsedArray = [];

    for (Map<String, dynamic> object in json) {
      parsedArray.add(ExtraDataObject(
          key: object['key'], value: object['value']));
    }

    return parsedArray;
  }
}
