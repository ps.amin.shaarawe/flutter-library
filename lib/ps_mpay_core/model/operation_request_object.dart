import 'dart:collection';

class OperationRequestKeys {
  static const receiver = 'receiver';
  static const receiverPin = 'receiverPin';
  static const receiverType = 'receiverType';
  static const notes = 'notes';
  static const amount = 'amount';
  static const promoCode = 'promoCode';
  static const sender = 'sender';
  static const pin = 'pin';
  static const operation = 'operation';
  static const deviceId = 'deviceId';
  static const lang = 'lang';
  static const senderType = 'senderType';
  static const tenant = 'tenant';
  static const msgId = 'msgId';
  static const extraData = 'extraData';
  static const sessionId = 'sessionId';
  static const checksum = 'checksum';
}

class OpreationRequestObject {
  String operation = '';
  String pinCode = '';
  String httpMethod = 'POST';
  List<SplayTreeMap<String, String>> extraData = [];
  String receiver = '';
  String receiverPin = '';
  String receiverType = '';
  String notes = '';
  String amount = '';
  String promoCode = '';
  String sessionId = '';
  String customMessageId;
  String mainMessageId;

  OpreationRequestObject({
    this.operation,
    this.pinCode,
    this.httpMethod,
    this.extraData,
    this.receiver,
    this.receiverPin,
    this.receiverType,
    this.notes,
    this.amount,
    this.promoCode,
    this.sessionId,
    this.customMessageId,
    this.mainMessageId,
  });

  OpreationRequestObject setup({
    String operation = '',
    String pinCode = '',
    String httpMethod = 'POST',
    List<SplayTreeMap<String, String>> extraData,
    String receiver = '',
    String receiverPin = '',
    String receiverType = '',
    String notes = '',
    String amount = '',
    String promoCode = '',
    String sessionId = '',
    String customMessageId,
  }) {
    OpreationRequestObject object = OpreationRequestObject(
      operation: operation,
      pinCode: pinCode,
      httpMethod: httpMethod,
      receiver: receiver,
      receiverPin: receiverPin,
      receiverType: receiverType,
      notes: notes,
      amount: amount,
      promoCode: promoCode,
      sessionId: sessionId,
      customMessageId: customMessageId,
    );

    if (extraData != null) {
      object.extraData = extraData;

      extraData.sort((a, b) {
        return a['key'].toLowerCase().compareTo(b['key'].toLowerCase());
      });
    } else {
      object.extraData = [];
    }

    return object;
  }
}
