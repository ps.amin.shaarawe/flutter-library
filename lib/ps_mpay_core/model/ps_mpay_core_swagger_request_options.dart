class SwaggerRequestOptions {
  static const showLoading = 'KEY_SHOW_LOADING';
  static const isReload = 'KEY_IS_RELOAD';
  static const context = 'KEY_CONTEXT';
  static const language = 'LANGUAGE';
  static const isHashed = 'IS_HASHED';
}
