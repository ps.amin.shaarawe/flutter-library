import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/ps_mpay_core_helper.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/ps_mpay_core_security_helper.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/operation_request_object.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/operation_response_object.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';

import 'http_configurations.dart';
import 'http_wrapper.dart';

class ConnectionManager {
  void sendRequestToServer(
      {OpreationRequestObject operationRequest,
      BuildContext context,
      bool showLoading = false,
      bool isReload = false,
      bool isTokenURL = false,
      bool isHashed = true,
      PsMpayCoreConnectionManagerCompletionHandler completionHandler,
      String filePath,
      HttpMethod httpMethod = HttpMethod.post}) async {
    String token = '';

    operationRequest.mainMessageId = await PsMpayCoreHelper().generateMessageId(
      customMessageId: operationRequest.customMessageId,
      operationName: operationRequest.operation,
    );
    if (Platform.isIOS) {
      token = await PsMpayCoreSecurityHelper()
          .getSigningTokenFromFlutter(operationRequest);
    } else {
      token = await PsMpayCoreSecurityHelper()
          .getSigningTokenFromAndroidNativeChannels(operationRequest);
    }

    var requestBody =
        await PsMpayCoreHelper().convertOperationRequestToMap(operationRequest);
    if (httpMethod == HttpMethod.multipart) {

      if (Platform.isIOS) {
        token = await PsMpayCoreSecurityHelper()
            .getPostBodySigningTokenFromFlutter(requestBody, isHashed: isHashed);
      } else {
        token = await PsMpayCoreSecurityHelper.shared
            .getPostBodySigningTokenFromAndroidNativeChannels(requestBody,
            isHashed: isHashed);
      }
    }
    final url =
        generateRequestUrl(httpMethod, isTokenURL, token); // + '?token=$token';

    HttpWrapper(
        url: url,
        postParameters: requestBody,
        requestType: httpMethod,
        encoding: utf8,
        context: context,
        showLoading: showLoading,
        customHeaders: (isTokenURL) ? null : {'token': token},
        file: filePath,
        resultHandler: (message, resultArray, status) {
          new Timer(
              Duration(
                  milliseconds: PsLibrarySetupObject
                      .shared.configDelayDuration), () async {
            if (status && resultArray.length > 0) {
              Uint8List responseBody = resultArray[1];

              final object = OperationResponseObject.fromJson(
                  json.decode(utf8.decode(responseBody)));

              final _errorCode = object.response.errorCd.toString();
              final _statusCode = object.response.statusCode.toString();
              final _status = _statusCode == '1' ||
                  _statusCode == '2' ||
                  _statusCode == 'ACCEPTED';

              completionHandler(
                result: [object],
                statusCode: _statusCode,
                errorCode: _errorCode,
                status: _status,
              );
            } else {
              completionHandler(
                result: [],
                statusCode: '',
                errorCode: '',
                status: false,
              );
            }
          });
        });
  }

  String generateRequestUrl(
      HttpMethod httpMethod, bool isTokenURL, String token) {
    return httpMethod == HttpMethod.post
        ? (isTokenURL)
            ? SharedParam.shared.baseUrl + '?token=$token'
            : SharedParam.shared.baseUrl
        : SharedParam.shared.uploadUrl;
  }
}
