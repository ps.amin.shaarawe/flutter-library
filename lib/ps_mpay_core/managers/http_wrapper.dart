import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';
// ignore: implementation_imports
import 'package:http_parser/src/media_type.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/alert_helper.dart';
import 'package:mpay_flutter_library/utils/ui/progress_hud.dart';
import 'http_configurations.dart';

typedef void ResponseCompletionHandler(
  String message,
  List result,
  bool status,
);

class HttpWrapper {
  Map<String, String> _headers = {'Content-Type': 'application/json'};

  String _url;
  bool _showLoading;
  bool _isTimeOutShown;
  dynamic _postParameters;
  Encoding _encoding;
  ResponseCompletionHandler _resultHandler;
  Duration _defaultTimeout;
  BuildContext _context;

  HttpWrapper(
      {String url,
      dynamic postParameters,
      HttpMethod requestType,
      Map<String, String> customHeaders,
      Encoding encoding,
      BuildContext context,
      bool showLoading,
      ResponseCompletionHandler resultHandler,
      String file}) {
    this._url = url;
    this._postParameters = postParameters;
    this._resultHandler = resultHandler;
    this._encoding = encoding;
    this._showLoading = showLoading;
    this._context = context;
    this._isTimeOutShown = false;
    this._defaultTimeout = Duration(seconds: 120);
    if (_showLoading) {
      ProgressHud.shared.startLoading(context);
    }

    if (customHeaders != null) {
      _headers.addAll(customHeaders);
    }

    if (context != null && context.widget != null) {
      _postByMethod(requestType, file);
    } else {
      _resultHandler(PsLibrarySetupObject.shared.generalError, [], false);
    }
  }

  void _postByMethod(HttpMethod requestType, String file) {
    switch (requestType) {
      case HttpMethod.post:
        _startPost();
        break;
      case HttpMethod.multipart:
        _startMultipartPost(file);
        break;
    }
  }

  void _startPost() async {
    try {
      final validateUrl = Uri.parse(this._url);

      if (!SharedParam.shared.isDebug && validateUrl.scheme != 'https') {
        AlertHelper.shared.showAlertDialog(
          message: PsLibrarySetupObject.shared.generalErrorMessage,
          context: this._context,
          completionHandler: () {
            exit(0);
          },
          alertType: AlertType.ERROR,
        );
        return;
      }

      final _clientContext = SecurityContext(withTrustedRoots: false);

      HttpClient client = new HttpClient(context: _clientContext)
        ..badCertificateCallback =
            ((X509Certificate cert, String host, int port) {
          if (SharedParam.shared.isDebug) return true;

          final pinningCertificate = SharedParam.shared.pinningCertificate;

          if (pinningCertificate.length == 0) {
            return false;
          }

          if (cert == null) {
            AlertHelper.shared.showAlertDialog(
                message: PsLibrarySetupObject.shared.generalErrorMessage,
                context: this._context,
                completionHandler: () {
                  exit(0);
                },
                alertType: AlertType.ERROR);
            return false;
          }

          String receivedPem = cert.pem
              .replaceAll('\n', '')
              .replaceAll(' ', '')
              .replaceAll('\r', '');
          String trusted = pinningCertificate
              .replaceAll('\n', '')
              .replaceAll(' ', '')
              .replaceAll('\r', '');
          bool isValid = receivedPem == trusted;

          if (!isValid) {
            AlertHelper.shared.showAlertDialog(
                message: PsLibrarySetupObject.shared.generalErrorMessage,
                context: this._context,
                completionHandler: () {
                  _startPost();
                },
                alertType: AlertType.ERROR);
            return false;
          }

          return isValid;
        });

      IOClient ioClient = new IOClient(client);

      final response = await ioClient
          .post(Uri.parse(this._url),
              headers: this._headers,
              body: this._postParameters,
              encoding: this._encoding)
          .timeout(_defaultTimeout, onTimeout: () {
        if (!this._isTimeOutShown) {
          this._isTimeOutShown = true;
          if (_showLoading) {
            ProgressHud.shared.stopLoading();
          }

          _printLog('Time out');
          _resultHandler(PsLibrarySetupObject.shared.timeoutTitle, [], false);
        }

        return;
      }).catchError((error) {
        if (_showLoading) {
          ProgressHud.shared.stopLoading();
        }
        _printLog('HTTP Exeption');
      });

      ioClient.close();

      if (!this._isTimeOutShown) {
        if (_showLoading) {
          ProgressHud.shared.stopLoading();
          Timer(
              Duration(
                  milliseconds:
                      PsLibrarySetupObject.shared.configDelayDuration), () {
            _handelResponse(response: response);
          });
        } else {
          _handelResponse(response: response);
        }
      }
    } catch (error) {
      if (_showLoading) {
        ProgressHud.shared.stopLoading();
      }
      _printLog('POST Request');
    }
  }

  void _startMultipartPost(String file) async {
    try {
      final validateUrl = Uri.parse(this._url);

      if (!SharedParam.shared.isDebug && validateUrl.scheme != 'https') {
        AlertHelper.shared.showAlertDialog(
          message: PsLibrarySetupObject.shared.generalErrorMessage,
          context: this._context,
          completionHandler: () {
            exit(0);
          },
          alertType: AlertType.ERROR,
        );
        return;
      }

      final _clientContext = SecurityContext(withTrustedRoots: false);

      HttpClient client = new HttpClient(context: _clientContext)
        ..badCertificateCallback =
            ((X509Certificate cert, String host, int port) {
          if (SharedParam.shared.isDebug) return true;

          final pinningCertificate = SharedParam.shared.pinningCertificate;

          if (pinningCertificate.length == 0) {
            return false;
          }

          if (cert == null) {
            AlertHelper.shared.showAlertDialog(
                message: PsLibrarySetupObject.shared.generalErrorMessage,
                context: this._context,
                completionHandler: () {
                  exit(0);
                },
                alertType: AlertType.ERROR);
            return false;
          }

          String receivedPem = cert.pem
              .replaceAll('\n', '')
              .replaceAll(' ', '')
              .replaceAll('\r', '');
          String trusted = pinningCertificate
              .replaceAll('\n', '')
              .replaceAll(' ', '')
              .replaceAll('\r', '');
          bool isValid = receivedPem == trusted;

          if (!isValid) {
            AlertHelper.shared.showAlertDialog(
                message: PsLibrarySetupObject.shared.generalErrorMessage,
                context: this._context,
                completionHandler: () {
                  _startPost();
                },
                alertType: AlertType.ERROR);
            return false;
          }

          return isValid;
        });

      IOClient ioClient = new IOClient(client);
      MultipartRequest multipartRequest = MultipartRequest(
        'POST',
        Uri.parse(this._url),
      );
      _headers['Content-Type'] = 'multipart/form-data';
      multipartRequest.headers.addAll(_headers);
      String key =
          await SecureStorageHelper().read(key: CacheKeys.devicePublicKey);

      multipartRequest.fields['content'] = _postParameters;
      multipartRequest.fields['key'] = key;
      multipartRequest.files.add(await MultipartFile.fromPath(
          'multipartFile', file,
          contentType: MediaType('application', 'octet-stream')));

      final responseStream = await ioClient
          .send(multipartRequest)
          .timeout(_defaultTimeout, onTimeout: () {
        if (!this._isTimeOutShown) {
          this._isTimeOutShown = true;
          if (_showLoading) {
            ProgressHud.shared.stopLoading();
          }

          _printLog('Time out');
          _resultHandler(PsLibrarySetupObject.shared.timeoutTitle, [], false);
        }

        return;
      }).catchError((error) {
        if (_showLoading) {
          ProgressHud.shared.stopLoading();
        }
        _printLog('HTTP Exeption');
      });

      var response = await Response.fromStream(responseStream);
      _printLog(response.body);

      ioClient.close();

      if (!this._isTimeOutShown) {
        if (_showLoading) {
          ProgressHud.shared.stopLoading();
          Timer(
              Duration(
                  milliseconds:
                      PsLibrarySetupObject.shared.configDelayDuration), () {
            _handelResponse(response: response);
          });
        } else {
          _handelResponse(response: response);
        }
      }
    } catch (error) {
      if (_showLoading) {
        ProgressHud.shared.stopLoading();
      }
      _printLog('POST Request');
    }
  }

  void _handelResponse({@required Response response}) async {
    if (_context.widget == null) {
      _resultHandler(
          PsLibrarySetupObject.shared.generalConnectionError, [], false);
      return;
    }

    final connectivityResult = await (Connectivity().checkConnectivity());

    try {
      _printLogWithResponse(response);
    } catch (e) {}

    if (response == null) {
      if (connectivityResult == null) {
        _resultHandler(
            PsLibrarySetupObject.shared.generalConnectionError, [], false);
      } else if (connectivityResult == ConnectivityResult.none) {
        _resultHandler(
            PsLibrarySetupObject.shared.generalConnectionError, [], false);
      } else {
        _resultHandler(
            PsLibrarySetupObject.shared.generalErrorMessage, [], false);
      }
    } else if (connectivityResult == null) {
      _resultHandler(
          PsLibrarySetupObject.shared.generalConnectionError, [], false);
    } else if (connectivityResult == ConnectivityResult.none) {
      _resultHandler(
          PsLibrarySetupObject.shared.generalConnectionError, [], false);
    } else if (response.statusCode == 200) {
      _resultHandler(PsLibrarySetupObject.shared.generalSuccess,
          [response.body, response.bodyBytes], true);
    } else {
      _resultHandler(
          PsLibrarySetupObject.shared.generalErrorMessage, [], false);
    }
  }

  void _printLogWithResponse(Response response) {
    if (response != null && SharedParam.shared.isDebug) {
      debugPrint('==============================================' +
          '\n' +
          'headers = ' +
          _headers.toString() +
          '\n\n' +
          'encoding = UTF8' +
          '\n\n' +
          'url =' +
          _url +
          '\n\n' +
          'postBody = ' +
          _postParameters +
          '\n\n' +
          'response body = ' +
          response.body);
      debugPrint('\n==============================================');
    }
  }

  void _printLog(String from) {
    if (SharedParam.shared.isDebug) {
      debugPrint('==============================================' +
          '\n' +
          'From : ' +
          from +
          '\n' +
          'headers = ' +
          _headers.toString() +
          '\n\n' +
          'encoding = UTF8' +
          '\n\n' +
          'url =' +
          _url +
          '\n\n' +
          'postBody = ' +
          _postParameters +
          '\n\n');
      debugPrint('\n==============================================');
    }
  }
}
