import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/ps_mpay_core_helper.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/ps_mpay_core_swagger_request_options.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/alert_helper.dart';
import 'package:mpay_flutter_library/utils/ui/progress_hud.dart';
import 'package:openapi_dart_common/openapi.dart';
import 'swagger_http_wrapper.dart';

class SwaggerConnectionManager extends ApiClientDelegate {
  @override
  Future<ApiResponse> invokeAPI(
      String basePath,
      String path,
      Iterable<QueryParam> queryParams,
      Object body,
      String jsonBody,
      options) async {
    bool _showLoading = options.extra[SwaggerRequestOptions.showLoading];
    if (_showLoading == null) {
      _showLoading = false;
    }

    bool _isReload = options.extra[SwaggerRequestOptions.isReload];

    if (_isReload == null) {
      _isReload = false;
    }

    bool _isHashed = options.extra[SwaggerRequestOptions.isHashed];

    if (_isHashed == null) {
      _isHashed = true;
    }

    final context = options.extra[SwaggerRequestOptions.context];

    if (_showLoading) ProgressHud.shared.startLoading(context);

    var _url = basePath + path;

    if (queryParams.length > 0) {
      _url = _url + '?';
      var _params = '';

      for (var counter = 0; counter < queryParams.length; counter++) {
        var queryParam = queryParams.toList()[counter];
        if (counter == 0) {
          _params = _params + queryParam.name + '=' + queryParam.value;
        } else {
          _params = _params + '&' + queryParam.name + '=' + queryParam.value;
        }
      }

      _url = _url + _params;
    }
    final _token = await PsMpayCoreHelper().generateToken(jsonBody, isHashed: _isHashed);

    final _settings = SwaggerHttpWrapper(
      url: _url,
      showLoading: true,
      postParameters: jsonBody,
      token: _token,
      currentLanguage: options.extra[SwaggerRequestOptions.language],
    );

    ApiResponse _response = ApiResponse();

    if (options.method.toString().toLowerCase() == 'post') {
      _response = await SwaggerHttpWrapper().post(wrapper: _settings);
    }

    if (options.method.toString().toLowerCase() == 'get') {
      _response = await SwaggerHttpWrapper().get(wrapper: _settings);
    }

    if (options.method.toString().toLowerCase() == 'put') {
      _response = await SwaggerHttpWrapper().put(wrapper: _settings);
    }

    if (_response.statusCode == null) _response.statusCode = 400;

    if (_response.statusCode == 403) _response.statusCode = 200;

    if (_response.statusCode >= 400 && _isReload) {
      final context = options.extra[SwaggerRequestOptions.context];
      var button = PsLibrarySetupObject.shared.okButtonTitle;
      var message = PsLibrarySetupObject.shared.generalError;

      final connectivityResult = await (Connectivity().checkConnectivity());

      if (connectivityResult == null ||
          connectivityResult == ConnectivityResult.none) {
        message = PsLibrarySetupObject.shared.generalConnectionError;
      }

      AlertHelper.shared.showAlertDialog(
          message: message,
          buttonLabel: button,
          context: context,
          completionHandler: () {
            new Timer(
                Duration(
                    milliseconds:
                        PsLibrarySetupObject.shared.configDelayDuration), () {
              invokeAPI(basePath, path, queryParams, body, jsonBody, options);
            });
          });
    }

    return _response;
  }
}
