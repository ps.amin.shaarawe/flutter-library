import 'dart:convert';
import 'dart:io';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/alert_helper.dart';
import 'package:openapi_dart_common/openapi.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';
import 'package:mpay_flutter_library/utils/ui/progress_hud.dart';

class SwaggerHttpWrapper {
  final String url;
  final bool showLoading;
  final dynamic postParameters;
  final Duration defaultTimeout = Duration(seconds: 120);
  final BuildContext context;
  final String token;
  final String currentLanguage;

  var headers = {
    'Content-Type': 'application/json',
    'accept': 'application/json',
  };

  SwaggerHttpWrapper({
    this.url,
    this.postParameters,
    this.context,
    this.token,
    this.currentLanguage,
    this.showLoading = true,
  });

  bool _validateUrl(SwaggerHttpWrapper wrapper) {
    final validateUrl = Uri.tryParse(wrapper.url);

    if (!SharedParam.shared.isDebug && validateUrl.scheme != 'https') {
      AlertHelper.shared.showAlertDialog(
          buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
          message: PsLibrarySetupObject.shared.generalError,
          context: wrapper.context,
          completionHandler: () {
            exit(0);
          });
      return false;
    }

    return true;
  }

  HttpClient _createClient(SwaggerHttpWrapper wrapper) {
    final _clientContext = SecurityContext(withTrustedRoots: false);
    wrapper.headers.addAll({'Accept-language': wrapper.currentLanguage});
    wrapper.headers.addAll({'token': wrapper.token});
    HttpClient _client = new HttpClient(context: _clientContext)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) {
        if (SharedParam.shared.isDebug) return true;

        final pinningCertificate = SharedParam.shared.pinningCertificate;

        if (pinningCertificate.length == 0) {
          return false;
        }

        if (cert == null) {
          AlertHelper.shared.showAlertDialog(
              buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
              message: PsLibrarySetupObject.shared.generalError,
              context: this.context,
              completionHandler: () {
                exit(0);
              },
              alertType: AlertType.ERROR);
          return false;
        }

        String receivedPem = cert.pem
            .replaceAll('\n', '')
            .replaceAll(' ', '')
            .replaceAll('\r', '');
        String trusted = pinningCertificate
            .replaceAll('\n', '')
            .replaceAll(' ', '')
            .replaceAll('\r', '');
        bool isValid = receivedPem == trusted;

        if (!isValid) {
          AlertHelper.shared.showAlertDialog(
              buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
              message: PsLibrarySetupObject.shared.generalError,
              context: this.context,
              completionHandler: () {
                exit(0);
              },
              alertType: AlertType.ERROR);
          return false;
        }

        return isValid;
      });

    return _client;
  }

  Future<ApiResponse> _finalResponse(
      SwaggerHttpWrapper wrapper, Response response) async {
    var _apiResponse = ApiResponse();
    _apiResponse.statusCode = 400;

    if (wrapper.showLoading) ProgressHud.shared.stopLoading();

    if (response == null) return _apiResponse;

    final connectivityResult = await (Connectivity().checkConnectivity());

    try {
      _printLogWithResponse(response, wrapper);
    } catch (e) {
      print(e.toString());
    }

    if (response == null) {
      if (connectivityResult == null)
        return _apiResponse;
      else if (connectivityResult == ConnectivityResult.none)
        return _apiResponse;
      else
        return _apiResponse;
    } else if (connectivityResult == null) {
      return _apiResponse;
    } else if (connectivityResult == ConnectivityResult.none) {
      return _apiResponse;
    } else {
      _apiResponse.statusCode = response.statusCode;
      _apiResponse.body = ByteStream.fromBytes(response.bodyBytes);

      return _apiResponse;
    }
  }

  Future<ApiResponse> put({SwaggerHttpWrapper wrapper}) async {
    var _apiResponse = ApiResponse();
    _apiResponse.statusCode = 400;

    try {
      final _validUrl = _validateUrl(wrapper);

      if (!_validUrl) {
        return _apiResponse;
      }

      var _isTimeout = false;

      HttpClient client = _createClient(wrapper);
      IOClient ioClient = new IOClient(client);

      final response = await ioClient
          .put(
        Uri.parse(wrapper.url),
        headers: wrapper.headers,
        body: wrapper.postParameters,
        encoding: utf8,
      )
          .timeout(defaultTimeout, onTimeout: () {
        if (!_isTimeout) {
          _isTimeout = true;
          if (wrapper.showLoading) {
            ProgressHud.shared.stopLoading();
          }

          _printLog('Time out', wrapper);
        }

        return;
      }).catchError((error) {
        if (wrapper.showLoading) {
          ProgressHud.shared.stopLoading();
        }
        _printLog('HTTP Exception', wrapper);
      });

      ioClient.close();

      if (!_isTimeout) {
        _apiResponse = await _finalResponse(wrapper, response);
      }
    } catch (error) {
      if (wrapper.showLoading) {
        ProgressHud.shared.stopLoading();
      }
      _printLog('PUT Request', wrapper);
    }

    return _apiResponse;
  }

  Future<ApiResponse> post({SwaggerHttpWrapper wrapper}) async {
    var _apiResponse = ApiResponse();
    _apiResponse.statusCode = 400;

    try {
      final _validUrl = _validateUrl(wrapper);

      if (!_validUrl) {
        return _apiResponse;
      }

      var _isTimeout = false;
      HttpClient client = _createClient(wrapper);
      IOClient ioClient = new IOClient(client);

      final response = await ioClient
          .post(
        Uri.parse(wrapper.url),
        headers: wrapper.headers,
        body: wrapper.postParameters,
        encoding: utf8,
      )
          .timeout(defaultTimeout, onTimeout: () {
        if (!_isTimeout) {
          _isTimeout = true;
          if (wrapper.showLoading) {
            ProgressHud.shared.stopLoading();
          }

          _printLog('Time out', wrapper);
        }

        return;
      }).catchError((error) {
        if (wrapper.showLoading) {
          ProgressHud.shared.stopLoading();
        }
        debugPrint('HTTP Exception $error');
      });
      ioClient.close();

      if (!_isTimeout) {
        _apiResponse = await _finalResponse(wrapper, response);
      }
    } catch (error) {
      if (wrapper.showLoading) {
        ProgressHud.shared.stopLoading();
      }
      _printLog('POST Request', wrapper);
    }

    return _apiResponse;
  }

  Future<ApiResponse> get({SwaggerHttpWrapper wrapper}) async {
    var _apiResponse = ApiResponse();
    _apiResponse.statusCode = 400;

    try {
      final _validUrl = _validateUrl(wrapper);

      if (!_validUrl) {
        return _apiResponse;
      }

      var _isTimeout = false;

      HttpClient client = _createClient(wrapper);
      IOClient ioClient = new IOClient(client);

      final response = await ioClient
          .get(Uri.parse(wrapper.url), headers: wrapper.headers)
          .timeout(defaultTimeout, onTimeout: () {
        if (!_isTimeout) {
          _isTimeout = true;
          if (wrapper.showLoading) {
            ProgressHud.shared.stopLoading();
          }

          _printLog('Time out', wrapper);
        }

        return;
      }).catchError((error) {
        if (wrapper.showLoading) {
          ProgressHud.shared.stopLoading();
        }
        _printLog('HTTP Exception', wrapper);
      });

      ioClient.close();

      if (!_isTimeout) {
        _apiResponse = await _finalResponse(wrapper, response);
      }
    } catch (error) {
      if (wrapper.showLoading) {
        ProgressHud.shared.stopLoading();
      }
      _printLog('POST Request', wrapper);
    }

    return _apiResponse;
  }

  void _printLogWithResponse(Response response, SwaggerHttpWrapper wrapper) {
    if (response != null && SharedParam.shared.isDebug) {
      debugPrint('==============================================' +
          '\n' +
          'headers = {\'Content-Type\' : \'application/json\'}' +
          '\n\n' +
          'encoding = UTF8' +
          '\n\n' +
          'url =' +
          wrapper.url +
          '\n\n' +
          'postBody = ' +
          wrapper.postParameters +
          '\n\n' +
          'response body = ' +
          response.body);
      debugPrint('\n==============================================');
    }
  }

  void _printLog(String from, SwaggerHttpWrapper wrapper) {
    if (SharedParam.shared.isDebug) {
      debugPrint('==============================================' +
          '\n' +
          'From : ' +
          from +
          '\n' +
          'headers = {\'Content-Type\' : \'application/json\'}' +
          '\n\n' +
          'encoding = UTF8' +
          '\n\n' +
          'url =' +
          wrapper.url +
          '\n\n' +
          'postBody = ' +
          wrapper.postParameters +
          '\n\n');
      debugPrint('\n==============================================');
    }
  }
}
