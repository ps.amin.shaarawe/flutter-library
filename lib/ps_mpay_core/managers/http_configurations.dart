import 'package:flutter/material.dart';

typedef void ResponseCompletionHandler(
    String message, List result, bool status);

class PsMpayCoreErrorCodes {
  static const success = '00';
  static const internalSystemError = '01';
  static const rejectedByNationalSwitch = '17';
  static const invalidClientIdNumber = '41';
  static const deviceNotFound = '45';
  static const invalidToken = '49';
  static const senderMobileBlocked = '72';
  static const deviceBlocked = '90';
  static const signOut = '91';
  static const expiredActivationCode = '131';
  static const invalidActivationCode = '43';
  static const invalidPinCode = '10';
  static const forgetPasswordInvalidOtp = '89';
  static const forgetPasswordInvalidClientNumber = '41';
  static const expiredForgetPasswordtOtp = '88';
}

typedef void PsMpayCoreConnectionManagerCompletionHandler({
  @required String statusCode,
  @required String errorCode,
  @required List result,
  @required bool status,
});

enum HttpMethod { post, multipart }
