import 'dart:convert';
import 'package:mpay_flutter_library/ps_mpay_core/model/system_configuration.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';

class SystemConfigurationHelper {
  static var shared = SystemConfigurationHelper();

  Future<SystemConfigurationObject> getSystemConfiguration(
      {String key = ''}) async {
    String value =
        await SecureStorageHelper().read(key: CacheKeys.systemConfigration);

    if (value.length == 0) {
      value = '[]';
    }

    List<SystemConfigurationObject> parsedArray =
        SystemConfigurationObject.init(json.decode(value))
            .where((object) => object.configKey == key)
            .toList();

    if (parsedArray.length > 0) {
      return parsedArray[0];
    }
    return SystemConfigurationObject();
  }
}
