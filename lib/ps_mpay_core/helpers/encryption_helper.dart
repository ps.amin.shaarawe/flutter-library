import 'package:encrypt/encrypt.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/ps_mpay_core_security_helper.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/signing_rsa_helper.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';
import "package:pointycastle/export.dart";

class FlutterEncryptionHelper {
  static var shared = FlutterEncryptionHelper();

  Future<String> encryptPlain(String plaintext) async {
    final keyPair = await getKeyPair();
    final encrypter = Encrypter(
        RSA(publicKey: keyPair.publicKey, privateKey: keyPair.privateKey));
    return encrypter.encrypt(plaintext).base64;
  }

  Future<String> decryptPlain(String cipherBase64) async {
    final keyPair = await getKeyPair();
    final encrypter = Encrypter(
        RSA(publicKey: keyPair.publicKey, privateKey: keyPair.privateKey));
    return encrypter.decrypt64(cipherBase64);
  }

  Future<AsymmetricKeyPair<PublicKey, PrivateKey>> getKeyPair() async {
    String savedPrivateKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePrivateKey);

    var helper = SigningRsaKeyHelper();
    var privateKey = helper.parsePrivateKeyFromPem(savedPrivateKey);

    String savedPublicKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePublicKey);
    var publicKey =
        await PsMpayCoreSecurityHelper().getPublicKey(savedPublicKey);

    return AsymmetricKeyPair(publicKey, privateKey);
  }
}
