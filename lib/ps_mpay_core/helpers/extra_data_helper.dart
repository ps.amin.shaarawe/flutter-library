import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/extra_data_object.dart';

class ExtraDataHelper {
  static var shared = ExtraDataHelper();

  SplayTreeMap<String, String> getExtraData({
    @required String key,
    @required String value,
  }) {
    SplayTreeMap<String, String> map = new SplayTreeMap();
    map['key'] = key.toString();
    map['value'] = value.toString();
    return map;
  }

  String getExtraDataFromKey({
    @required String key,
    @required List<ExtraDataObject> extraData,
  }) {
    if (extraData == null) {
      return '';
    }

    for (var obj in extraData) {
      if (obj.key.toString() == key) {
        return obj.value.toString();
      }
    }
    return '';
  }
}
