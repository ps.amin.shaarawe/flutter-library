import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mpay_flutter_library/library_setup/library_setup.dart';
import 'package:mpay_flutter_library/utils/ui/custom_alert/alert_helper.dart';
import 'package:mpay_flutter_library/utils/ui/textfield_helper.dart';

class DialogWithEditText {
  void showAlertWithTextField(BuildContext context,
      {String body,
      TextEditingController textEditingController,
      int codeLength,
      String label,
      TextInputType keyboardType = TextInputType.number,
      VoidCallback cancelHandler,
      VoidCallback completion}) {
    AlertHelper.shared.showAlertDialog(
        message: body,
        context: context,
        isCancelButton: true,
        buttonLabel: PsLibrarySetupObject.shared.okButtonTitle,
        cancelCompletionHandler: cancelHandler,
        customWidget: _getTextField(
            context, textEditingController, keyboardType, codeLength, label),
        completionHandler: completion);
  }

  Widget _getTextField(
      BuildContext context,
      TextEditingController textEditingController,
      TextInputType textInputType,
      int codeLength,
      String label) {
    return TextFieldHelper.shared.createGradientField(
      keyboardType: textInputType,
      controller: textEditingController,
      context: context,
      onChanged: (value) {
        if (value.length == codeLength) {
          FocusScope.of(context).requestFocus(new FocusNode());
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        }
      },
      inputFormatters: [
        FilteringTextInputFormatter(new RegExp(' '), allow: false),
        LengthLimitingTextInputFormatter(codeLength),
      ],
      labelText: label,
      onSubmitted: (v) {
        FocusScope.of(context).requestFocus(new FocusNode());
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      prefixIcon: Icon(Icons.dialpad, color: Colors.white),
      obscureText: true,
    );
  }
}
