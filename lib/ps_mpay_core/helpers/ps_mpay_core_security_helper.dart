import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:mpay_flutter_library/configurations/native_channels.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/ps_mpay_core_helper.dart';
import 'package:mpay_flutter_library/ps_mpay_core/helpers/signing_rsa_helper.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/operation_request_object.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';

class DigestSink extends Sink<Digest> {
  Digest get value {
    assert(_value != null);
    return _value;
  }

  Digest _value;

  @override
  void add(Digest value) {
    assert(_value == null);
    _value = value;
  }

  @override
  void close() {
    assert(_value != null);
  }
}

class PsMpayCoreSecurityHelper {
  static PsMpayCoreSecurityHelper shared = PsMpayCoreSecurityHelper();

  getPublicKey(String encryptionSecret) async {
    var publicKey =
        SigningRsaKeyHelper().parsePublicKeyFromPem(encryptionSecret);
    return publicKey;
  }

  getPrivateKey(String key) async {
    var privateKey = SigningRsaKeyHelper().parsePrivateKeyFromPem(key);
    return privateKey;
  }

  String getHashedMessage({@required String plain}) {
    var message = utf8.encode(plain);

    var digestSink = new DigestSink();

    var chunkedConversion = sha256.startChunkedConversion(digestSink);
    chunkedConversion.add(message);
    chunkedConversion.close();

    var digest = digestSink.value;
    var hashedString = base64Encode(digest.bytes); // we need to use this

    return hashedString;
  }

  String getHashedMessageWithReplaceArabic({@required String plain}) {
    plain = PsMpayCoreHelper.shared.replaceArabicNumbers(containsArabic: plain);
    var message = utf8.encode(plain);

    var digestSink = new DigestSink();

    var chunkedConversion = sha256.startChunkedConversion(digestSink);
    chunkedConversion.add(message);
    chunkedConversion.close();

    var digest = digestSink.value;
    var hashedString = base64Encode(digest.bytes); // we need to use this

    return hashedString;
  }

  Future<String> rsaEncryption({String plain, String encryptionSecret}) async {
    var publicKey = await getPublicKey(encryptionSecret);

    if (publicKey == null) {
      return '';
    }

    String token = SigningRsaKeyHelper().encrypt(plain, publicKey);
    return Uri.encodeComponent(token);
  }

  Future<String> rsaEncryptionWithoutEncodeComponent({
    @required String plain,
    @required String encryptionSecret,
  }) async {
    var publicKey = await getPublicKey(encryptionSecret);

    if (publicKey == null) {
      return '';
    }

    String token = SigningRsaKeyHelper().encrypt(plain, publicKey);
    return token;
  }

  Future<String> rsaDecryption({@required String encrypted}) async {
    if (Platform.isAndroid) {
      final MethodChannel channel =
          const MethodChannel(PsMpayFrameworkNativeChannels.mainChannel);
      final String decrypted = await channel.invokeMethod(
          PsMpayFrameworkNativeChannels.decryptRsa, encrypted);
      return decrypted;
    }

    String savedPrivateKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePrivateKey);

    if (savedPrivateKey.isEmpty) {
      await generateKeyPairsFromFlutter();
      savedPrivateKey =
          await SecureStorageHelper().read(key: CacheKeys.devicePrivateKey);
    }

    var privateKey = await getPrivateKey(savedPrivateKey);

    if (privateKey == null) {
      return '';
    }

    String decrypted = SigningRsaKeyHelper().decrypt(encrypted, privateKey);
    return decrypted;
  }

  Future<String> rsaEncryptionWithDeviceKeyPair(
      {@required String plain}) async {
    if (Platform.isAndroid) {
      final MethodChannel channel =
          const MethodChannel(PsMpayFrameworkNativeChannels.mainChannel);
      final String encrypted = await channel.invokeMethod(
          PsMpayFrameworkNativeChannels.encryptRsa, plain);
      return encrypted;
    }

    String savedPublicKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePublicKey);

    if (savedPublicKey.isEmpty) {
      await generateKeyPairsFromFlutter();
      savedPublicKey =
          await SecureStorageHelper().read(key: CacheKeys.devicePublicKey);
    }

    var publicKey = await getPublicKey(savedPublicKey);

    if (publicKey == null) {
      return '';
    }

    String encrypted = SigningRsaKeyHelper().encrypt(plain, publicKey);
    return encrypted;
  }

  Future<void> generateKeyPairsFromFlutter() async {
    String savedPrivateKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePrivateKey);

    if (savedPrivateKey.isEmpty) {
      final helper = SigningRsaKeyHelper();
      var keyPair = await helper
          .computeRSAKeyPair(SigningRsaKeyHelper().getSecureRandom());
      await SecureStorageHelper().save(
          key: CacheKeys.devicePublicKey,
          value: helper.encodePublicKeyToPem(keyPair.publicKey));
      await SecureStorageHelper().save(
          key: CacheKeys.devicePrivateKey,
          value: helper.encodePrivateKeyToPem(keyPair.privateKey));
    }
  }

  Future<String> getSigningTokenFromFlutter(
      OpreationRequestObject operationRequest) async {
    var msgString = await PsMpayCoreHelper().getStringBuilder(operationRequest);

    var hashedString = getHashedMessage(plain: msgString);

    if (SharedParam.shared.isDebug) {
      print('msgString = ' + msgString);
      print('hashedString = ' + hashedString);
    }

    String savedPrivateKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePrivateKey);

    if (savedPrivateKey.isEmpty) {
      return 'INVALID_TOKEN';
    }

    var helper = SigningRsaKeyHelper();
    String token = helper.sign(
        hashedString, helper.parsePrivateKeyFromPem(savedPrivateKey));
    return Uri.encodeComponent(token);
  }

  Future<String> getSigningTokenFromAndroidNativeChannels(
      OpreationRequestObject operationRequest) async {
    var msgString = await PsMpayCoreHelper().getStringBuilder(operationRequest);
    var hashedString = getHashedMessage(plain: msgString);

    if (SharedParam.shared.isDebug) {
      print('msgString = ' + msgString);
      print('hashedString = ' + hashedString);
    }

    try {
      final MethodChannel channel =
          const MethodChannel(PsMpayFrameworkNativeChannels.mainChannel);
      final String token = await channel.invokeMethod(
          PsMpayFrameworkNativeChannels.signText, hashedString);
      return Uri.encodeComponent(token);
    } catch (exception) {
      if (SharedParam.shared.isDebug) print('Share error: $exception');
    }
    return 'empty';
  }

  Future<void> generateKeyPairsFromAndroidNativeChannels() async {
    String savedPublicKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePublicKey);

    if (savedPublicKey.isEmpty) {
      try {
        final MethodChannel channel =
            const MethodChannel(PsMpayFrameworkNativeChannels.mainChannel);
        await channel.invokeMethod(
            PsMpayFrameworkNativeChannels.signText, 'generate');

        String publicKey = await channel.invokeMethod(
            PsMpayFrameworkNativeChannels.getPublicKey, 'generate');
        // publicKey = '-----BEGIN PUBLIC KEY-----\n' +
        //     publicKey +
        //     '\n-----END PUBLIC KEY-----';

        await SecureStorageHelper()
            .save(key: CacheKeys.devicePublicKey, value: publicKey);
      } catch (exception) {}
    }
  }

  Future<String> getPostBodySigningTokenFromAndroidNativeChannels(
      String msgString, {bool isHashed = true}) async {

    String hashedString = msgString;
    if (isHashed) hashedString = getHashedMessage(plain: msgString);
    try {
      final channel =
          const MethodChannel(PsMpayFrameworkNativeChannels.mainChannel);
      final String token = await channel.invokeMethod(
          PsMpayFrameworkNativeChannels.signText, hashedString);
      return (token);
    } catch (exception) {
      print('Share error: $exception');
    }
    return 'empty';
  }

  Future<String> getPostBodySigningTokenFromFlutter(String msgString, {bool isHashed = true}) async {
    String hashedString = msgString;
    if (isHashed) hashedString = getHashedMessage(plain: msgString);

    if (SharedParam.shared.isDebug) {
      print('hashedString = ' + hashedString.toString());
    }

    String savedPrivateKey =
        await SecureStorageHelper().read(key: CacheKeys.devicePrivateKey);

    if (savedPrivateKey.isEmpty) {
      return 'INVALID_TOKEN';
    }

    var helper = SigningRsaKeyHelper();
    String token =
        helper.sign(hashedString, helper.parsePrivateKeyFromPem(savedPrivateKey));

    if (SharedParam.shared.isDebug) {
      print('token = ' + token.toString());
    }

    return token;
  }
}
