import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/operation_request_object.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:uuid/uuid.dart';
import 'language_id.dart';
import 'ps_mpay_core_security_helper.dart';

class PsMpayCoreHelper {
  static final shared = PsMpayCoreHelper();

  convertOperationRequestToMap(
      OpreationRequestObject operationRequest) async {
    SplayTreeMap requestJson = new SplayTreeMap();

    if (operationRequest.receiver.length > 0) {
      requestJson[OperationRequestKeys.receiver] =
          operationRequest.receiver;
    }

    if (operationRequest.receiverPin.length > 0) {
      requestJson[OperationRequestKeys.receiverPin] =
          operationRequest.receiverPin;
    }

    if (operationRequest.receiverType.length > 0) {
      requestJson[OperationRequestKeys.receiverType] =
          operationRequest.receiverType;
    }

    if (operationRequest.notes.length > 0) {
      requestJson[OperationRequestKeys.notes] =
          operationRequest.notes;
    }

    if (operationRequest.amount.length > 0) {
      requestJson[OperationRequestKeys.amount] =
          operationRequest.amount;
    }

    if (SharedParam.shared.checksum.length > 0) {
      requestJson[OperationRequestKeys.checksum] =
          SharedParam.shared.checksum;
    }

    if (operationRequest.promoCode.length > 0) {
      requestJson[OperationRequestKeys.promoCode] =
          operationRequest.promoCode;
    }

    final _sessionId =
        await SecureStorageHelper().read(key: CacheKeys.sessionId);

    if (_sessionId.length == 0 &&
        SharedParam.shared.sender.length > 0) {
      requestJson[OperationRequestKeys.sender] =
          SharedParam.shared.sender;
    }

    requestJson[OperationRequestKeys.senderType] =
        SharedParam.shared.senderType;

    if (_sessionId.length > 0) {
      requestJson[OperationRequestKeys.sessionId] = _sessionId;
    }

    if (operationRequest.pinCode.length > 0) {
      requestJson[OperationRequestKeys.pin] =
          operationRequest.pinCode;
    }

    requestJson[OperationRequestKeys.operation] =
        operationRequest.operation;

    if (_sessionId.length == 0) {
      requestJson[OperationRequestKeys.deviceId] =
          SharedParam.shared.deviceId;
    }

    requestJson[OperationRequestKeys.lang] =
        int.parse(getLanguageId());

    requestJson[OperationRequestKeys.tenant] =
        SharedParam.shared.tenant;

    requestJson[OperationRequestKeys.msgId] =
        operationRequest.mainMessageId;

    if (operationRequest.extraData.length > 0) {
      requestJson[OperationRequestKeys.extraData] =
          operationRequest.extraData;
    }

    return json.encode(requestJson);
  }

  Future<String> generateMessageId({
    String customMessageId,
    String operationName = '',
  }) async {
    if (customMessageId != null) {
      return customMessageId;
    }

    var messageId = Uuid().v4().toString().replaceAll('-', '');

    if (messageId.length > 34) {
      messageId = messageId.substring(0, 34);
    }

    if (SharedParam.shared.isDebug)
      print('========================> ' + messageId);

    return messageId;
  }

  String getRandomString(int length) {
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();
    return String.fromCharCodes(Iterable.generate(
        length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  }

  Future<String> getStringBuilder(
      OpreationRequestObject operationRequest) async {
    var stringBuilder = '';

    if (operationRequest.amount.toString().replaceAll('null', '').length > 0) {
      stringBuilder += operationRequest.amount;
    }

    if (SharedParam.shared.checksumValue.length > 0) {
      stringBuilder += SharedParam.shared.checksumValue;
    }

    final _sessionId =
        await SecureStorageHelper().read(key: CacheKeys.sessionId);

    if (_sessionId.length == 0) {
      stringBuilder += SharedParam.shared.deviceId;
    }

    String _langId = getLanguageId();
    stringBuilder += _langId;
    stringBuilder += operationRequest.mainMessageId;

    if (operationRequest.notes.length > 0) {
      stringBuilder += operationRequest.notes;
    }

    stringBuilder += operationRequest.operation;

    if (operationRequest.pinCode.length > 0) {
      stringBuilder += operationRequest.pinCode;
    }

    if (operationRequest.promoCode.length > 0) {
      stringBuilder += operationRequest.promoCode;
    }

    if (operationRequest.receiver.length > 0) {
      stringBuilder += operationRequest.receiver;
    }

    if (operationRequest.receiverPin.length > 0) {
      stringBuilder += operationRequest.receiverPin;
    }

    if (operationRequest.receiverType.length > 0) {
      stringBuilder += operationRequest.receiverType;
    }

    if (_sessionId.length == 0) {
      stringBuilder += SharedParam.shared.sender;
    }

    stringBuilder += SharedParam.shared.senderType;

    if (_sessionId.length > 0) {
      stringBuilder += _sessionId;
    }

    stringBuilder += SharedParam.shared.tenant;

    if (operationRequest.extraData != null) {
      for (Map<String, String> obj in operationRequest.extraData) {
        String value = obj['value'];
        //String key = obj['key'];

        //if (key != 'key' && value != null) {
        stringBuilder += value;
        //}
      }
    }

    return stringBuilder;
  }

  String getLanguageId() {
    String _lang = SharedParam.shared.lang;

    if (_lang == null) _lang = 'en';

    if (_lang.length == 0) _lang = 'en';

    if (_lang == 'ar') return LanguageId.arabic;

    if (_lang == 'fr') return LanguageId.french;

    return LanguageId.english;
  }

  String replaceArabicNumbers({@required String containsArabic}) {
    return containsArabic
        .replaceAll('٠', '0')
        .replaceAll('١', '1')
        .replaceAll('٢', '2')
        .replaceAll('٣', '3')
        .replaceAll('٤', '4')
        .replaceAll('٥', '5')
        .replaceAll('٦', '6')
        .replaceAll('٧', '7')
        .replaceAll('٨', '8')
        .replaceAll('٩', '9')
        .replaceAll('۰',
            '0') // dont remove the duplicate please (asci code is different)
        .replaceAll('۱', '1')
        .replaceAll('۲', '2')
        .replaceAll('۳', '3')
        .replaceAll('۴', '4')
        .replaceAll('۵', '5')
        .replaceAll('۶', '6')
        .replaceAll('۷', '7')
        .replaceAll('۸', '8')
        .replaceAll('۹', '9')
        .replaceAll('*', '')
        .replaceAll('#', '')
        .replaceAll(',', '')
        .replaceAll(';', '')
        .replaceAll('+', '')
        .replaceAll('٫', '.');
  }

  Future<String> generateToken(String msgString, {bool isHashed = true}) async {
    String token = '';
    if (Platform.isIOS) {
      token = await PsMpayCoreSecurityHelper()
          .getPostBodySigningTokenFromFlutter(msgString, isHashed: isHashed);
    } else {
      token = await PsMpayCoreSecurityHelper()
          .getPostBodySigningTokenFromAndroidNativeChannels(msgString, isHashed: isHashed);
    }

    return token;
  }
}
