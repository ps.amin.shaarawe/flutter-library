import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:mpay_flutter_library/ps_mpay_core/model/shared_param.dart';
import 'package:mpay_flutter_library/utils/helpers/secure_storage_helper.dart';
import 'package:mpay_flutter_library/utils/helpers/shared_prefrences_helper.dart';
import 'package:mpay_flutter_library/configurations/cache_keys.dart';
import 'package:uuid/uuid.dart';

class DeviceHelper {
  static DeviceHelper shared = DeviceHelper();

  Future<String> getDeviceId() async {
    String deviceId = '';

    final deviceInfo = DeviceInfoPlugin();

    if (Platform.isAndroid) {
      final _isSkipIntro =
          await SharedPreferencesHelper().read(key: CacheKeys.isSkipIntroOld);

      if (_isSkipIntro == 'yes') {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        if (androidInfo != null) {
          deviceId = androidInfo.androidId;
        }
      } else {
        var _deviceId =
            await SecureStorageHelper().read(key: CacheKeys.deviceId);

        if (_deviceId.isEmpty) {
          _deviceId = generateDeviceId();
          await SecureStorageHelper()
              .save(key: CacheKeys.deviceId, value: _deviceId);
        }

        deviceId = _deviceId;
      }
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      if (iosInfo != null) {
        deviceId = iosInfo.identifierForVendor;
      }
    }

    return deviceId;
  }

  Future<bool> checkIsPhysicalDevice() async {
    bool isPhysicalDevice = false;

    final deviceInfo = DeviceInfoPlugin();

    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (androidInfo != null) {
        isPhysicalDevice = androidInfo.isPhysicalDevice;
      }
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      if (iosInfo != null) {
        isPhysicalDevice = iosInfo.isPhysicalDevice;
      }
    }

    return isPhysicalDevice;
  }

  String generateDeviceId() {
    var deviceId = Uuid().v4();

    if (SharedParam.shared.isDebug)
      print('======================== Device Id ========================> ' +
          deviceId);

    return deviceId;
  }
}
