import 'package:flutter/material.dart';

class PsLibrarySetupObject {
  static var shared = PsLibrarySetupObject();

  final Color borderColor;
  final Color primaryColor;
  final String contactUsPhone;
  final String generalErrorMessage;
  final String currency;
  final int fractionDigits;
  final String callTitle;
  final String okButtonTitle;
  final String cancelButtonLabel;
  final String timeoutTitle;
  final String didntReceive;
  final String clickToResend;
  final String generalConnectionError;
  final String generalConfirmationMessage;
  final String generalSuccess;
  final String generalError;
  final Gradient globalGradient;
  final Gradient accentGradient;
  final double minWidthForFontModification;
  final String doneButtonTitle;
  final bool isArabic;
  final String amountRex;
  final int configDelayDuration;
  final String nextButtonLabel;
  final Widget customSpinKit;

  PsLibrarySetupObject({
    this.isArabic = false,
    this.borderColor = Colors.black,
    this.primaryColor = Colors.black,
    this.contactUsPhone = '',
    this.configDelayDuration = 200,
    this.callTitle = '',
    this.okButtonTitle = '',
    this.cancelButtonLabel = '',
    this.timeoutTitle = '',
    this.didntReceive = '',
    this.clickToResend = '',
    this.generalConnectionError = '',
    this.generalConfirmationMessage = 'Confirmation Message',
    this.generalSuccess = '',
    this.generalError = '',
    this.globalGradient = const LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: [Colors.black, Colors.black, Colors.black],
    ),
    this.accentGradient = const LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: [Colors.white, Colors.white, Colors.white],
    ),
    this.minWidthForFontModification = 400,
    this.doneButtonTitle = '',
    this.amountRex = '',
    this.generalErrorMessage = '',
    this.currency = '',
    this.fractionDigits = 2,
    this.nextButtonLabel = '',
    this.customSpinKit,
  });
}
