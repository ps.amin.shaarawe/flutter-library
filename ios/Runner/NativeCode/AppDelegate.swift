import UIKit
import Flutter
import AVFoundation
import AudioToolbox
//import Firebase
let MAIN_CHANNEL = "MAIN_CHANNEL"
let METHOD_SHARE_QR = "METHOD_SHARE_QR"
let METHOD_PLAY_BEEP_SOUND = "METHOD_PLAY_BEEP_SOUND"

//@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        GeneratedPluginRegistrant.register(with: self)
        self.initMainChannel()
//        FirebaseApp.configure()
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func initMainChannel(){
        
        guard let controller = self.window.rootViewController as? FlutterViewController else { return }
        guard let binaryMessenger = controller as? FlutterBinaryMessenger else { return }
        
        let shareChannel = FlutterMethodChannel(name: MAIN_CHANNEL, binaryMessenger: binaryMessenger)
        
        shareChannel.setMethodCallHandler { (call, result) in
            if call.method == METHOD_SHARE_QR , let arguments = call.arguments as? String {
                let nsDocumentDirectory = FileManager.SearchPathDirectory.cachesDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath = paths.first {
                    
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(arguments)
                    guard let image = UIImage(contentsOfFile: imageURL.path) else { return }
                    
                    let itemsToShare:[Any] = [image]
                    let activityViewController = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
                    
                    if let popoverPresentationController = activityViewController.popoverPresentationController {
                        popoverPresentationController.sourceView = controller.view // so that iPads won't crash
                    }
                    
                    // present the view controller
                    controller.present (activityViewController, animated: true, completion: nil)
                }
            }
            
            if call.method == METHOD_PLAY_BEEP_SOUND {
                guard let filePath = Bundle.main.path(forResource: "beep", ofType: "mp3") else { return }
                
                let soundURL = NSURL(fileURLWithPath: filePath)
                var soundID: SystemSoundID = 0
                
                AudioServicesCreateSystemSoundID(soundURL, &soundID)
                AudioServicesPlaySystemSound(soundID)
            }
        }
    }
    
    override func applicationWillResignActive(_ application: UIApplication) {
        if let image = UIImage(named: "logo") {
            let view = UIView()
            view.frame = self.window.bounds
            view.backgroundColor = UIColor(red: 207/255, green: 10/255, blue: 44/255, alpha: 1)
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFit
            imageView.frame = CGRect(x: 0, y: self.window.frame.height/2-50, width: self.window.frame.width, height: 100)
            view.addSubview(imageView)
            view.tag = 10001
            self.window.addSubview(view)
        }
    }
    
    override func applicationDidBecomeActive(_ application: UIApplication) {
        if let view = self.window.viewWithTag(10001) {
            view.removeFromSuperview()
        }
    }
    
}
