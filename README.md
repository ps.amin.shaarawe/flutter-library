<!-- # MPAY READ ME INSTRUCTIONS
Use this command to generate localizations : flutter pub run intl_utils:generate

If the pod file delete or rm ios/Podfile used, then add the below pods again:
  pod 'Firebase/Analytics'
  pod 'Firebase/Crashlytics'

KEYSTORE DETAILS
name: meps_keystore_release
password: meps_keystore_release@P@ssw0rd
alias: meps_keystore_release_alias
alias password: meps_keystore_release@P@ssw0rd

Huawei Certificate DETAILS
keytool -export -rfc -keystore meps_keystore_release -alias meps_keystore_release_alias -file meps_keystore_release.pem

# UPLOAD TO STORES CHECKLIST

# General checks for push new release to the store:
 - isDebug should be false
 - isDemo should be false
 - Set live environment to live in class "EnvironmentConfigurations" ==> Environment currentEnvironment = Environment.live;
 - Double check any static username or password in "LoginScreen"
 - Double the live pinning certificate
 - Double the live encryption certificate

# Appstore (Apple)
1- Do All general checks.
2- Double check google firebase info.plist to match the live bundle.
3- Create push notification entitlement for the live bundle using the live account.
4- Double check App Uses Non-Exempt Encryption to be set to NO in info.plist.
5- Ask the customer for the whats new in case it is update version and update the store with it (All provided languages for example Arabic and English).
6- Update the review notes section with the updated OTP and credentials.
7- Update the version.
8- Update the build number, first one should be "1".
9- Update the store listing if there are any new data for example screenshots.
10- Apple static national id for Apple review that named  "appStoreTestingNid" to be updated.

# GooglePlay (Android)
1- Do All general checks.
2- Double check google firebase json file to match the live package id.
3- Ask the customer for the whats new in case it is update version and update the store with it (All provided languages for example Arabic and English).
4- Update the version.
5- Update the build number, first one should be "1".
6- Update the store listing if there are any new data for example screenshots.

# Huawei (Android)
1- Do All general checks.
2- Double check google firebase json file to match the live package id.
3- Ask the customer for the whats new in case it is update version and update the store with it (All provided languages for example Arabic and English).
4- Update the version.
5- Update the build number, first one should be "1".
6- Update the store listing if there are any new data for example screenshots.
7- Huawei static national id for Huawei review that named "huaweiTestingNid" to be updated.

--!>
